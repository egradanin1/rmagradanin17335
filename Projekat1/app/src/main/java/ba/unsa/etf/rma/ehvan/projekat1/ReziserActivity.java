package ba.unsa.etf.rma.ehvan.projekat1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

public class ReziserActivity extends AppCompatActivity {

    public ReziserActivity() {

    }

    Button dugme;
    Button dugme1;
    Button dugme2;
    ListView lista;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reziser);

        dugme = (Button) findViewById(R.id.rbutton);
        dugme1 = (Button) findViewById(R.id.rbutton1);
        dugme2 = (Button) findViewById(R.id.rbutton2);
        lista = (ListView) findViewById(R.id.rlist);


        Reziser r=new Reziser("Steven","Spielberg");
        Reziser r1=new Reziser("Michael","Bay");
        Reziser r2=new Reziser("Jason","Blum");
        final ArrayList<Reziser> unosi = new ArrayList<Reziser>();
        unosi.add(r);
        unosi.add(r1);
        //unosi.add(r2);


        final ReziserAdapter adapter = new ReziserAdapter(this, unosi);


        lista.setAdapter(adapter);
        dugme.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                Intent myIntent = new Intent(ReziserActivity.this, MainActivity.class);
                ReziserActivity.this.startActivity(myIntent);
            }
        });
        dugme2.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                Intent myIntent = new Intent(ReziserActivity.this, ZanrActivity.class);
                ReziserActivity.this.startActivity(myIntent);
            }
        });
    }
}