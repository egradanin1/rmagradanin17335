package ba.unsa.etf.rma.ehvan.projekat1;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.view.View;
import android.widget.Toast;


public class detalji extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Resources res=getResources();
        setContentView(R.layout.activity_detalji);
        final TextView imeiprezime = (TextView)findViewById(R.id.atextView1);
        imeiprezime.setText(getIntent().getStringExtra("ime") + " " +getIntent().getStringExtra("prezime"));
        final TextView rodjenje=(TextView)findViewById(R.id.atextView2);
        rodjenje.setText("Rodjen-a: "+getIntent().getStringExtra("rodjenje"));
        final TextView smrt=(TextView)findViewById(R.id.atextView3);
        if(getIntent().getStringExtra("smrt").equals("ziv"))
            smrt.setText("");
        else
            smrt.setText("Umro: "+getIntent().getStringExtra("smrt"));
        final TextView biografija=(TextView)findViewById(R.id.atextView4);
        biografija.setText("O glumcu-ici: "+getIntent().getStringExtra("biografija")+" Rejting: "+getIntent().getIntExtra("rejting",0));
        final TextView mjesto=(TextView)findViewById(R.id.atextView5);
        mjesto.setText("Mjesto rodjenja: "+getIntent().getStringExtra("mjesto"));
        final TextView spol=(TextView)findViewById(R.id.atextView6);
        if(getIntent().getBooleanExtra("spol",true)==true){
            spol.setText("Spol: Musko");
            getWindow().getDecorView().setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        }
        else{
            spol.setText("Spol: Zensko");
            getWindow().getDecorView().setBackgroundColor(ContextCompat.getColor(this, R.color.colorAccent));
        }

        final TextView stranica=(TextView)findViewById(R.id.atextView7);
        stranica.setText(getIntent().getStringExtra("stranica"));
        ImageView slika=(ImageView)findViewById(R.id.aimageView);
        slika.setImageResource(res.getIdentifier(getIntent().getStringExtra("slika"),null,null));
        Button dugme = (Button)findViewById(R.id.dbutton);
        dugme.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {

                String textMessage= imeiprezime.getText()+" "+rodjenje.getText()+" "+smrt.getText()+" "
                +biografija.getText()+" "+mjesto.getText()+" "+spol.getText().toString()+" "+stranica.getText();
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, textMessage);
                sendIntent.setType("text/plain");

                if (sendIntent.resolveActivity(getPackageManager()) != null) {
                    startActivity(sendIntent);
                }
            }
        });
    }

    public void klik(View view) {
        TextView tekst = (TextView)findViewById(R.id.atextView7);
        String url = tekst.getText().toString();
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }
}
