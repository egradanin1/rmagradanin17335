package ba.unsa.etf.rma.ehvan.projekat1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

public class ZanrActivity extends AppCompatActivity {
    public ZanrActivity() {

    }

    Button dugme;
    Button dugme1;
    Button dugme2;
    ListView lista;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zanr);

        dugme = (Button) findViewById(R.id.zbutton);
        dugme1 = (Button) findViewById(R.id.zbutton1);
        dugme2 = (Button) findViewById(R.id.zbutton2);
        lista = (ListView) findViewById(R.id.zlist);


       Zanr z=new Zanr("komedija");
        z.setImage("ba.unsa.etf.rma.ehvan.projekat1:drawable/" + z.getNaziv());
        Zanr z1=new Zanr("horor");
        z.setImage("ba.unsa.etf.rma.ehvan.projekat1:drawable/" + z.getNaziv());
        Zanr z2=new Zanr("action");
        z2.setImage("ba.unsa.etf.rma.ehvan.projekat1:drawable/" + z.getNaziv());
        final ArrayList<Zanr> unosi = new ArrayList<Zanr>();
        unosi.add(z);
        unosi.add(z1);
        //unosi.add(z2);



        final ZanrAdapter adapter = new ZanrAdapter(this, unosi, getResources());


        lista.setAdapter(adapter);
        dugme.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                Intent myIntent = new Intent(ZanrActivity.this, MainActivity.class);
                ZanrActivity.this.startActivity(myIntent);
            }
        });
        dugme1.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                Intent myIntent = new Intent(ZanrActivity.this, ReziserActivity.class);
                ZanrActivity.this.startActivity(myIntent);
            }
        });

    }
}
