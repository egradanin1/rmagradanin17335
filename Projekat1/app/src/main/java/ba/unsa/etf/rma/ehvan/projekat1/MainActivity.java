package ba.unsa.etf.rma.ehvan.projekat1;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {
    public MainActivity()
    {

    }

    Button dugme;
    Button dugme1;
    Button dugme2;
    ListView lista;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dugme = (Button)findViewById(R.id.button);
        dugme1 = (Button)findViewById(R.id.button1);
        dugme2 = (Button)findViewById(R.id.button2);
        lista = (ListView)findViewById(R.id.list);




        Glumac g=(new Glumac("enis","Beslagic","1975","ziv",true,"https://www.imdb.com/name/nm0078692/","Podrijetlom je iz Tešnja. Diplomirao je na Akademiji scenskih umjetnosti u Sarajevu 2001. godine. Ostvario je uloge u više od 30 predstava. Najveću popularnost kod hrvatske publike stekao je nastupajući u seriji Naša mala klinika, te sa serijalom snimljenih viceva o Muji i Hasi.","Tesanj",5));
        g.setImage("ba.unsa.etf.rma.ehvan.projekat1:drawable/"+g.getIme());
        Glumac g1=(new Glumac("angelina","Jolie","1975","ziv",false,"http://www.imdb.com/name/nm0001401/?ref_=nv_sr_1","Rođena je u Los Angelesu, u glumačkoj obitelji. Otac joj je glumac Jon Voight, poznat po filmovima Ponoćni kauboj (1969.) i Povratak veterana (1978.) za kojeg je dobio Oscara, a majka Marcheline Bertrand, bivša manekenka i glumica. Njeni roditelji su se razveli kada je Angelina imala godinu dana. ","Los Angeles",5));
        g1.setImage("ba.unsa.etf.rma.ehvan.projekat1:drawable/"+g1.getIme());
        Glumac g2=(new Glumac("brad","Pitt","1987","ziv",true,"http://www.imdb.com/name/nm0000093/?ref_=nv_sr_1","William Bradley Brad Pitt (Shawnee, Oklahoma, 18. prosinca 1963.), američki filmski glumac, producent i aktivist. Postao je slavan sredinom devedesetih nakon pojavljivanja u nekoliko velikih holivudskih filmova.","Shawneej",5));
        g2.setImage("ba.unsa.etf.rma.ehvan.projekat1:drawable/"+g2.getIme());
        final ArrayList<Glumac> unosi = new ArrayList<Glumac>();
        unosi.add(g);
        unosi.add(g1);
        //unosi.add(g2);


        final GlumacAdapter adapter = new GlumacAdapter(this,unosi,getResources());


        lista.setAdapter(adapter);

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l){

                Intent intent = new Intent(MainActivity.this, detalji.class);
                intent.putExtra("ime",unosi.get(i).getIme());
                intent.putExtra("prezime",unosi.get(i).getPrezime());
                intent.putExtra("biografija",unosi.get(i).getBiografija());
                intent.putExtra("stranica",unosi.get(i).getWebStranica());
                intent.putExtra("rodjenje",unosi.get(i).getRodjenje());
                intent.putExtra("smrt",unosi.get(i).getSmrt());
                intent.putExtra("mjesto",unosi.get(i).getMjesto());
                intent.putExtra("spol",unosi.get(i).getSpol());
                intent.putExtra("rejting",unosi.get(i).getRejting());
                intent.putExtra("slika",unosi.get(i).getImage());
                startActivity(intent);
            }
        });

        dugme1.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                Intent myIntent = new Intent(MainActivity.this, ReziserActivity.class);
                MainActivity.this.startActivity(myIntent);
            }
        });
        dugme2.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                Intent myIntent = new Intent(MainActivity.this, ZanrActivity.class);
                MainActivity.this.startActivity(myIntent);
            }
        });


    }

}
