package ba.unsa.etf.rma.ehvan.projekat1;

/**
 * Created by lion on 3/28/17.
 */

public class Zanr {
    private String naziv;
    private String image;

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Zanr(String naziv) {
        this.naziv = naziv;
    }
}
