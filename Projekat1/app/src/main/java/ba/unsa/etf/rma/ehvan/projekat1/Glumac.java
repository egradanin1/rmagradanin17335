package ba.unsa.etf.rma.ehvan.projekat1;

/**
 * Created by lion on 3/26/17.
 */

public class Glumac {
    private String ime;
    private String prezime;
    private  String rodjenje;
    private String smrt;
    private Boolean spol;
    private String webStranica;
    private String biografija;
    private String mjesto;
    private Integer rejting;
    private  String image="";

    public Glumac(String i, String p, String r,String s,Boolean sp,String w, String b ,String m,Integer ra){
        ime=i;
        prezime=p;
        rodjenje=r;
        smrt=s;
        spol=sp;
        webStranica=w;
        biografija=b;
        mjesto=m;
        rejting=ra;
    }
    public void setImage(String i){image=i;}
    public  void setRejting(Integer ra){rejting=ra;}

    public void setIme(String i){
        ime=i;
    }

    public void setPrezime(String p){
        prezime=p;
    }

    public void setRodjenje(String r){rodjenje=r;}

    public void setSmrt(String s){smrt=s;}

    public void setSpol(Boolean sp){spol=sp;}

    public void setWebStranica(String w){
        webStranica=w;
    }

    public void setBiografija(String b){
        biografija=b;
    }

    public void setMjesto(String m){mjesto=m;}

    public String getIme(){
        return ime;
    }

    public String getPrezime(){
        return prezime;
    }

    public String getRodjenje(){
        return rodjenje;
    }

    public String getSmrt(){return  smrt;}

    public Boolean getSpol() { return spol;}

    public String getWebStranica(){
        return webStranica;
    }

    public String getBiografija(){
        return biografija;
    }

    public String getMjesto() {return  mjesto;}

    public Integer getRejting(){return  rejting;}

    public String getImage(){return  image;}
}
