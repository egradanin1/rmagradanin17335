package ba.unsa.etf.rma.ehvan.projekat1;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by lion on 3/28/17.
 */

public class ZanrAdapter extends ArrayAdapter<Zanr>{
    Resources res;
    public ZanrAdapter(@NonNull Context context, ArrayList<Zanr> m, Resources r){
        super(context, R.layout.zanr, m);
        res=r;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater li = LayoutInflater.from(getContext());
        View temp = li.inflate(R.layout.zanr,parent,false);


        Zanr zanr = getItem(position);

        TextView tekst = (TextView) temp.findViewById(R.id.ztextView);

        ImageView slika = (ImageView) temp.findViewById(R.id.zimageView);

        tekst.setText(zanr.getNaziv());

        slika.setImageResource(res.getIdentifier("ba.unsa.etf.rma.ehvan.projekat1:drawable/"+zanr.getNaziv(),null,null));

        return temp;
    }
}
