package ba.unsa.etf.rma.ehvan.projekat1;

/**
 * Created by lion on 3/28/17.
 */

public class Reziser {
    private String ime;
    private String prezime;
    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }



    public Reziser(String ime, String prezime) {
        this.ime = ime;
        this.prezime = prezime;
    }
}
