package ba.unsa.etf.rma.ehvan.projekat2;
import android.content.Intent;
import android.view.View;
import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.FrameLayout;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements  FragmentListaGlumaca.OnItemClick,FragmentDugme.UpravljanjeFragmentima,FragmentDugme2.UpravljanjeFragmentima2{
    ArrayList<Glumac> unosi=new ArrayList<>();
    final ArrayList<Zanr> unosi1 = new ArrayList<Zanr>();
    final ArrayList<Reziser> unosi2 = new ArrayList<Reziser>();
    Boolean siriL;
    Boolean detalji;
    int zadnji;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null){
            unosi = savedInstanceState.getParcelableArrayList("glumci");
            zadnji=savedInstanceState.getInt("zadnji");
        }

            setContentView(R.layout.activity_main);
            FragmentManager fm = getFragmentManager();
            // FragmentListaGlumaca f1=(FragmentListaGlumaca)fm.findFragmentByTag("Glumci");

            GlumacAdapter adapt = new GlumacAdapter(this, unosi);
           // Glumac g = (new Glumac("enis Beslagic", "1975", "ziv", "Musko", "https://www.imdb.com/name/nm0078692/", "Podrijetlom je iz Tešnja. Diplomirao je na Akademiji scenskih umjetnosti u Sarajevu 2001. godine. Ostvario je uloge u više od 30 predstava. Najveću popularnost kod hrvatske publike stekao je nastupajući u seriji Naša mala klinika, te sa serijalom snimljenih viceva o Muji i Hasi.", "Tesanj", "5"));
            //g.setImage(R.drawable.enis);
            // Glumac g1 = (new Glumac("angelina Jolie", "1975", "ziv", "Zensko", "http://www.imdb.com/name/nm0001401/?ref_=nv_sr_1", "Rođena je u Los Angelesu, u glumačkoj obitelji. Otac joj je glumac Jon Voight, poznat po filmovima Ponoćni kauboj (1969.) i Povratak veterana (1978.) za kojeg je dobio Oscara, a majka Marcheline Bertrand, bivša manekenka i glumica. Njeni roditelji su se razveli kada je Angelina imala godinu dana. ", "Los Angeles", "5"));
            // g1.setImage(R.drawable.angelina);

            //unosi.add(g);
            //unosi.add(g1);
            ZanrAdapter za = new ZanrAdapter(this, unosi1);


            ReziserAdapter ra = new ReziserAdapter(this, unosi2);


            FrameLayout det = (FrameLayout) findViewById(R.id.mjestoF3);
            if (det != null)
                siriL = true;
            else
                siriL = false;
            if (!siriL) {
                FragmentDugme fd = (FragmentDugme) fm.findFragmentById(R.id.mjestoF1);
                if (fd == null) {
                    fd = new FragmentDugme();
                    fm.beginTransaction().replace(R.id.mjestoF1, fd).commit();
                }
                FragmentListaGlumaca FLG= new FragmentListaGlumaca();


                Bundle arg2 = new Bundle();
                arg2.putParcelableArrayList("Alista", unosi);
                FLG.setArguments(arg2);

                fm.beginTransaction().replace(R.id.mjestoF2, FLG).commit();

            } else {
                FragmentDugme2 fd2 = (FragmentDugme2) fm.findFragmentById(R.id.Dugmad2);
                if (fd2 == null) {
                    fd2 = new FragmentDugme2();
                    fm.beginTransaction().replace(R.id.Dugmad2, fd2).commit();
                }
                FragmentListaGlumaca FLG= new FragmentListaGlumaca();

                    Bundle arg = new Bundle();
                    arg.putParcelableArrayList("Alista", unosi);
                    FLG.setArguments(arg);
                    if (!getFragmentManager().beginTransaction().isEmpty())
                        getFragmentManager().popBackStack();
                    fm.beginTransaction().replace(R.id.Lista2, FLG).commit();


                Bundle arg2 = new Bundle();
                if (!unosi.isEmpty())
                    arg2.putParcelable("glumac", unosi.get(0));
                FragmentDetalji fd = new FragmentDetalji();
                fd.setArguments(arg2);
                if (!getFragmentManager().beginTransaction().isEmpty())
                    getFragmentManager().popBackStack();
                getFragmentManager().beginTransaction().replace(R.id.mjestoF3, fd).addToBackStack(null).commit();

            }



    }
    @Override
    public void onItemClicked(int pos){
        zadnji=pos;
        if(siriL==false) {
            detalji = true;
            Bundle arg = new Bundle();

            arg.putParcelable("glumac", unosi.get(pos));
            FragmentDetalji fd = new FragmentDetalji();
            fd.setArguments(arg);
            if (!getFragmentManager().beginTransaction().isEmpty())
                getFragmentManager().popBackStack();
            getFragmentManager().beginTransaction().replace(R.id.mjestoF2, fd).addToBackStack(null).commit();
        }
        else {
            detalji = false;
            Bundle arg = new Bundle();
            arg.putParcelable("glumac", unosi.get(pos));
            FragmentDetalji fd = new FragmentDetalji();
            fd.setArguments(arg);
            if (!getFragmentManager().beginTransaction().isEmpty())
                getFragmentManager().popBackStack();
            getFragmentManager().beginTransaction().replace(R.id.mjestoF3, fd).addToBackStack(null).commit();
        }
    }
    @Override
    public void KlikniNaGlumca() {

        Bundle argumenti = new Bundle();
        detalji=false;
        argumenti.putParcelableArrayList("Alista",unosi);
        FragmentListaGlumaca fg = new FragmentListaGlumaca();
        fg.setArguments(argumenti);
        getFragmentManager().popBackStack();
        getFragmentManager().beginTransaction().replace(R.id.mjestoF2,fg).commit();
    }

    @Override
    public void KlikniNaZanr() {
        Bundle argumenti = new Bundle();
        argumenti.putString("id", unosi.get(zadnji).getID());
        argumenti.putParcelableArrayList("Blista",unosi1);
        FragmentListaZanrova fz = new FragmentListaZanrova();
        fz.setArguments(argumenti);
        getFragmentManager().popBackStack();
        getFragmentManager().beginTransaction().replace(R.id.mjestoF2,fz).commit();
    }

    @Override
    public void KlikniNaRezisera() {
        Bundle argumenti = new Bundle();
        argumenti.putString("id", unosi.get(zadnji).getID());
        argumenti.putParcelableArrayList("Clista",unosi2);
        FragmentListaRezisera fr = new FragmentListaRezisera();
        fr.setArguments(argumenti);
        getFragmentManager().popBackStack();
        getFragmentManager().beginTransaction().replace(R.id.mjestoF2,fr).commit();
    }
    @Override
    public void KlikniNaGlumce(){
        detalji=false;

        FragmentListaGlumaca FLG=new FragmentListaGlumaca();
        Bundle argumenti=new Bundle();
        argumenti.putParcelableArrayList("Alista",unosi);
        FLG.setArguments(argumenti);
        getFragmentManager().beginTransaction().replace(R.id.Lista2,FLG).addToBackStack(null).commit();
        Bundle argumenti2=new Bundle();
        if(!unosi.isEmpty())
            argumenti2.putParcelable("glumac",unosi.get(0));
        FragmentDetalji fd=new FragmentDetalji();
        fd.setArguments(argumenti2);
        getFragmentManager().beginTransaction().replace(R.id.mjestoF3,fd).addToBackStack(null).commit();

    }
    @Override
    public void KlikniNaOstalo(){
        detalji=false;
        Bundle argumenti=new Bundle();
        argumenti.putString("id", unosi.get(zadnji).getID());
        argumenti.putParcelableArrayList("Clista",unosi2);
        FragmentListaRezisera FLR=new FragmentListaRezisera();
        FLR.setArguments(argumenti);
        getFragmentManager().beginTransaction().replace(R.id.Lista2,FLR).addToBackStack(null).commit();
        Bundle argumenti2=new Bundle();
        argumenti2.putString("id", unosi.get(zadnji).getID());
        argumenti2.putParcelableArrayList("Blista",unosi1);
        FragmentListaZanrova FLZ=new FragmentListaZanrova();
        FLZ.setArguments(argumenti2);
        getFragmentManager().beginTransaction().replace(R.id.mjestoF3,FLZ).addToBackStack(null).commit();

    }
    @Override
    protected void onSaveInstanceState(Bundle icicle) {
        super.onSaveInstanceState(icicle);
        icicle.putParcelableArrayList("glumci", unosi);
        icicle.putInt("zadnji",zadnji);
    }
    @Override
    public void onBackPressed(){
        if(siriL)
            finish();
        else{
            if(!detalji)
                finish();
            else{
                getFragmentManager().popBackStack();
                detalji=false;
            }
        }
    }

}
