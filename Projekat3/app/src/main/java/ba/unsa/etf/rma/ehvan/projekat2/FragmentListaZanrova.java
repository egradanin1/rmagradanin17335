package ba.unsa.etf.rma.ehvan.projekat2;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by lion on 4/12/17.
 */

public class FragmentListaZanrova extends Fragment implements CustomResultReceiver.Receiver{
    ArrayList<Zanr>zanrovi;
    int id;
    public CustomResultReceiver.Receiver funkcija(){
        return FragmentListaZanrova.this;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup containter, Bundle savedInstanceState){
        return inflater.inflate(R.layout.listazanrova_fragment,containter,false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
       if (getArguments().containsKey("Blista")) {
            zanrovi = getArguments().getParcelableArrayList("Blista");
            ListView lv = (ListView) getView().findViewById(R.id.zlist);
            ZanrAdapter za = new ZanrAdapter(getActivity(),zanrovi);
            lv.setAdapter(za);
        }
        zanrovi.removeAll(zanrovi);
        //new SearchGenres((SearchGenres.onGenresSearchDone) FragmentListaZanrova.this).execute(getArguments().getString("id"));
        Intent intent = new Intent(Intent.ACTION_SYNC, null, getActivity(), GenresIntentService.class);
        CustomResultReceiver mReceiver = new CustomResultReceiver(new Handler());
        mReceiver.setReceiver(funkcija());
        intent.putExtra("ime",getArguments().getString("id"));
        intent.putExtra("receiver", mReceiver);
        getActivity().startService(intent);
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {


        switch (resultCode) {

            case GenresIntentService.STATUS_RUNNING:

                Toast.makeText(getActivity(), "Pretrazivanje u toku", Toast.LENGTH_LONG).show();
                break;
            case GenresIntentService.STATUS_FINISHED:

                ArrayList<Zanr> rez = resultData.getParcelableArrayList("result");
                for (Zanr g:rez
                        ) {
                    zanrovi.add(g);
                }
                ListView lv=(ListView)getView().findViewById(R.id.zlist);
                ZanrAdapter za=new ZanrAdapter(getActivity(),zanrovi);
                lv.setAdapter(za);
                Toast.makeText(getActivity(), "Pretrazivanje gotovo", Toast.LENGTH_LONG).show();
                break;
            case GenresIntentService.STATUS_ERROR:

                String error = resultData.getString(Intent.EXTRA_TEXT);
                Toast.makeText(getActivity(), "Doslo je do greske!", Toast.LENGTH_LONG).show();
                break;
        }

    }
}
