package ba.unsa.etf.rma.ehvan.projekat2;

/**
 * Created by lion on 4/12/17.
 */

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class ZanrAdapter extends ArrayAdapter<Zanr>{
    int res;
    public ZanrAdapter(@NonNull Context context, ArrayList<Zanr> m){
        super(context, R.layout.zanr, m);

    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater li = LayoutInflater.from(getContext());
        View temp = li.inflate(R.layout.zanr,parent,false);


        Zanr zanr = getItem(position);

        TextView tekst = (TextView) temp.findViewById(R.id.ztextView);



        tekst.setText(zanr.getNaziv());



        return temp;
    }
}
