package ba.unsa.etf.rma.ehvan.projekat2;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by lion on 4/12/17.
 */

public class Zanr implements Parcelable {
    private String naziv;
    private Integer image;

    public  Zanr(Parcel in){
        naziv=in.readString();



    }
    public static final Creator<Zanr> CREATOR=new Creator<Zanr>() {
        @Override
        public Zanr createFromParcel(Parcel source) {
            return new  Zanr(source);
        }

        @Override
        public Zanr[] newArray(int size) {
            return new Zanr[size];
        }
    };
    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public Integer getImage() {
        return image;
    }

    public void setImage(Integer image) {
        this.image = image;
    }



    public Zanr(String naziv) {
        this.naziv = naziv;

    }
    @Override
    public int describeContents(){
        return  0;
    }
    @Override
    public void writeToParcel(Parcel dest,int flags){
        dest.writeString(naziv);



    }
}
