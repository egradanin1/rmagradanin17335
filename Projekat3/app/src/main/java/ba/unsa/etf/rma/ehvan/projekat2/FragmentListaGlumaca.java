package ba.unsa.etf.rma.ehvan.projekat2;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;
import android.os.Handler;

/**
 * Created by lion on 4/12/17.
 */

public class FragmentListaGlumaca extends Fragment implements  CustomResultReceiver.Receiver{
    OnItemClick oic;
    private ArrayList<Glumac> glumci;

    public CustomResultReceiver.Receiver funkcija(){
        return FragmentListaGlumaca.this;
    }
    public ArrayList<Glumac> getGlumci() {
        return glumci;
    }

    public void setGlumci(ArrayList<Glumac> glumci) {
        this.glumci = glumci;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup containter, Bundle savedInstanceState){
        return inflater.inflate(R.layout.listaglumaca_fragment,containter,false);
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        Button dugme=(Button) getView().findViewById(R.id.Pretrazi);
        final EditText tekst=(EditText)getView().findViewById(R.id.pretraga);
        dugme.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {

                Intent intent = new Intent(Intent.ACTION_SYNC, null, getActivity(), ActorsIntentService.class);
                CustomResultReceiver mReceiver = new CustomResultReceiver(new Handler());
                mReceiver.setReceiver(funkcija());
                intent.putExtra("ime",tekst.getText().toString());
                intent.putExtra("receiver", mReceiver);
                getActivity().startService(intent);
                //new SearchActors((SearchActors.onActorSearchDone) FragmentListaGlumaca.this).execute(tekst.getText().toString());
            }
        });

        if(getArguments().containsKey("Alista")){
            glumci=getArguments().getParcelableArrayList("Alista");
            ListView lv=(ListView)getView().findViewById(R.id.list);
            GlumacAdapter ga=new GlumacAdapter(getActivity(),glumci);
            lv.setAdapter(ga);


        try{
            oic=(OnItemClick)getActivity();
        }
        catch (ClassCastException e){
            throw  new ClassCastException(getActivity().toString()+"Treba implementirati OnItemClick");

        }
            lv.setOnItemClickListener(new AdapterView.OnItemClickListener(){
                @Override
                public void onItemClick(AdapterView<?> parent,View view,int position,long id){
                    oic.onItemClicked(position);
                }
            });
        }

    }
    public interface OnItemClick{
        public void onItemClicked(int pos);
    }


    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {


            switch (resultCode) {


                case ActorsIntentService.STATUS_RUNNING:

                    Toast.makeText(getActivity(), "Pretrazivanje u toku", Toast.LENGTH_LONG).show();
                    break;
                case ActorsIntentService.STATUS_FINISHED:
                    glumci.removeAll(glumci);
                    ArrayList<Glumac> rez = resultData.getParcelableArrayList("result");

                    for (Glumac g:rez
                            ) {
                        glumci.add(g);
                    }
                    ListView lv=(ListView)getView().findViewById(R.id.list);
                    GlumacAdapter ga=new GlumacAdapter(getActivity(),glumci);
                    lv.setAdapter(ga);
                    Toast.makeText(getActivity(), "Pretrazivanje gotovo", Toast.LENGTH_LONG).show();
                    break;
                case ActorsIntentService.STATUS_ERROR:

                    String error = resultData.getString(Intent.EXTRA_TEXT);
                    Toast.makeText(getActivity(), "Doslo je do greske!", Toast.LENGTH_LONG).show();
                    break;
            }

    }
}
