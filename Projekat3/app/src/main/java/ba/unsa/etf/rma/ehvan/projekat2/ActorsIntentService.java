package ba.unsa.etf.rma.ehvan.projekat2;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by lion on 5/15/17.
 */

public class ActorsIntentService extends IntentService {
    public static final int STATUS_RUNNING = 0;
    public static final int STATUS_FINISHED = 1;
    public static final int STATUS_ERROR = 2;

    public ActorsIntentService() {
        super(null);
    }

    public ActorsIntentService(String name) {
        super(name);

    }

    @Override
    public void onCreate() {
        super.onCreate();

    }

    ArrayList<Glumac> rez=new ArrayList<>();
    @Override
    protected void onHandleIntent(Intent intent) {


        final ResultReceiver receiver = intent.getParcelableExtra("receiver");
        Bundle bundle = new Bundle();

        /* Update UI: Početak taska */
        receiver.send(STATUS_RUNNING, Bundle.EMPTY);


        String query = null;
        query=intent.getStringExtra("ime");
        JSONArray results=null;
        int velicina=0;
        String imeiprezime;
        String rodjenje;
        String smrt;
        String spol;
        String webStranica;
        String biografija;
        String mjesto;
        String rejting;
        String image=null;
        String id=null;
        if(query.contains(" ")){
            int poz=query.indexOf(" ");
            query=query.replaceAll(" ","+");
        }
        String url1="https://api.themoviedb.org/3/search/person?api_key=bcf63027b3451894a3574dbb2b436f82&query="+query;

        try{

            URL url = new URL(url1);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in=null;



            try {

                in = new BufferedInputStream(urlConnection.getInputStream());

            }
            catch (Exception e){
                Log.i("exception",e.getMessage().toString());
            }
            String rezultat=convertStreamToString(in);

            JSONObject jo=new JSONObject(rezultat );
            results=jo.getJSONArray("results");
            if(results.length()<7)
                velicina=results.length();
            else
                velicina=7;
            //stavljeno radi testiranja
            //Log.i("ID",id);
            Log.i("velicina",String.valueOf(velicina));

        }
        catch (MalformedURLException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        for(int j=0;j<velicina;j++) {


            try {
                JSONObject actor = results.getJSONObject(j);
                id = actor.getString("id");
                String url2 = "https://api.themoviedb.org/3/person/" + id + "?api_key=bcf63027b3451894a3574dbb2b436f82&language=en-US";


                URL url = new URL(url2);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = null;


                try {

                    in = new BufferedInputStream(urlConnection.getInputStream());

                } catch (Exception e) {
                    Log.i("exception", e.getMessage().toString());
                }
                String rezultat = convertStreamToString(in);

                JSONObject jo = new JSONObject(rezultat);


                imeiprezime = jo.getString("name");
                rodjenje = jo.getString("birthday");
                smrt = jo.getString("deathday");
                if (smrt.isEmpty())
                    smrt = "ziv";
                if (jo.getString("gender").equals("2"))
                    spol = "Musko";
                else
                    spol = "Zensko";
                webStranica = "http://www.imdb.com/name/" + jo.getString("imdb_id") + "/";

                biografija = jo.getString("biography");
                int i = biografija.indexOf(".");
                biografija = biografija.substring(0, i) + ".";
                mjesto = jo.getString("place_of_birth").substring(0, jo.getString("place_of_birth").indexOf(","));
                rejting = jo.getString("popularity");
                image = "https://image.tmdb.org/t/p/w640" + jo.getString("profile_path");
                Glumac g = new Glumac(imeiprezime, rodjenje, smrt, spol, webStranica, biografija, mjesto, rejting);
                g.setImage(image);
                g.setID(id);
                rez.add(g);

                //Log.i("detalji",imeiprezime+" "+ rodjenje+ " " + smrt + " "+ spol+ " "+ webStranica+" "+ biografija+" "+mjesto+" "+rejting+" "+image);

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.getMessage().toString();
            }
            bundle.putParcelableArrayList("result", rez);

        }
        receiver.send(STATUS_FINISHED, bundle);
    }

    public String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
        } finally {
            try {
                is.close();
            } catch (IOException e) {
            }
        }
        return sb.toString();
    }
}
