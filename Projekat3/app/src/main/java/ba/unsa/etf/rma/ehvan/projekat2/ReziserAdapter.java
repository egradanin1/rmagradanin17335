package ba.unsa.etf.rma.ehvan.projekat2;

/**
 * Created by lion on 4/12/17.
 */
import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class ReziserAdapter  extends ArrayAdapter<Reziser>{
    int res;
    public ReziserAdapter(@NonNull Context context, ArrayList<Reziser> m){
        super(context, R.layout.reziser, m);

    }
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater li = LayoutInflater.from(getContext());
        View temp = li.inflate(R.layout.reziser,parent,false);


        Reziser reziser = getItem(position);

        TextView tekst = (TextView) temp.findViewById(R.id.rtextView);
        tekst.setText(reziser.getImeiPrezime());

        return temp;
    }
}
