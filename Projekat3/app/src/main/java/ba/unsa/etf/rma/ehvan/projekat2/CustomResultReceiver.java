package ba.unsa.etf.rma.ehvan.projekat2;

import android.os.Bundle;
import android.os.ResultReceiver;

/**
 * Created by lion on 5/15/17.
 */

public class CustomResultReceiver extends ResultReceiver{
    private Receiver mReceiver;
    public CustomResultReceiver(android.os.Handler handler) {
        super(handler);
    }
    public void setReceiver(Receiver receiver) {
        mReceiver = receiver;
    }
    /* Deklaracija interfejsa koji će se trebati implementirati */
    public interface Receiver {
        public void onReceiveResult(int resultCode, Bundle resultData);
    }
    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        if (mReceiver != null) {
            mReceiver.onReceiveResult(resultCode, resultData);
        }
    }
}
