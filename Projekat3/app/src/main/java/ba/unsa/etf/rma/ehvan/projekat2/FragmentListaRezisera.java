package ba.unsa.etf.rma.ehvan.projekat2;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by lion on 4/12/17.
 */

public class FragmentListaRezisera extends Fragment implements CustomResultReceiver.Receiver {
    ArrayList<Reziser>reziseri;
    public CustomResultReceiver.Receiver funkcija(){
        return FragmentListaRezisera.this;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup containter, Bundle savedInstanceState){
        return inflater.inflate(R.layout.listarezisera_fragment,containter,false);
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getArguments().containsKey("Clista")) {
            reziseri = getArguments().getParcelableArrayList("Clista");
            ListView lv = (ListView) getView().findViewById(R.id.rlist);
            ReziserAdapter ra = new ReziserAdapter(getActivity(), reziseri);
            lv.setAdapter(ra);
        }
        reziseri.removeAll(reziseri);
        //new SearchDirectors((SearchDirectors.onDirectorSearchDone) FragmentListaRezisera.this).execute(getArguments().getString("id"));
        Intent intent = new Intent(Intent.ACTION_SYNC, null, getActivity(), DirectorsIntentService.class);
        CustomResultReceiver mReceiver = new CustomResultReceiver(new Handler());
        mReceiver.setReceiver(funkcija());
        intent.putExtra("ime",getArguments().getString("id"));
        intent.putExtra("receiver", mReceiver);
        getActivity().startService(intent);
    }


    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {


        switch (resultCode) {

            case DirectorsIntentService.STATUS_RUNNING:

                Toast.makeText(getActivity(), "Pretrazivanje u toku", Toast.LENGTH_LONG).show();
                break;
            case DirectorsIntentService.STATUS_FINISHED:

                ArrayList<Reziser> rez = resultData.getParcelableArrayList("result");
                for (Reziser g:rez
                        ) {
                    reziseri.add(g);
                }
                ListView lv=(ListView)getView().findViewById(R.id.rlist);
                ReziserAdapter ra=new ReziserAdapter(getActivity(),reziseri);
                lv.setAdapter(ra);
                Toast.makeText(getActivity(), "Pretrazivanje gotovo", Toast.LENGTH_LONG).show();
                break;
            case DirectorsIntentService.STATUS_ERROR:

                String error = resultData.getString(Intent.EXTRA_TEXT);
                Toast.makeText(getActivity(), "Doslo je do greske!", Toast.LENGTH_LONG).show();
                break;
        }

    }

}
