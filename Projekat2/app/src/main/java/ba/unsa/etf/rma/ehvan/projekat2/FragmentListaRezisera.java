package ba.unsa.etf.rma.ehvan.projekat2;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Created by lion on 4/12/17.
 */

public class FragmentListaRezisera extends Fragment {
    ArrayList<Reziser>reziseri;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup containter, Bundle savedInstanceState){
        return inflater.inflate(R.layout.listarezisera_fragment,containter,false);
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getArguments().containsKey("Clista")) {
            reziseri = getArguments().getParcelableArrayList("Clista");
            ListView lv = (ListView) getView().findViewById(R.id.rlist);
            ReziserAdapter ra = new ReziserAdapter(getActivity(), reziseri);
            lv.setAdapter(ra);
        }
    }

}
