package ba.unsa.etf.rma.ehvan.projekat2;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by lion on 4/12/17.
 */

public class Reziser implements Parcelable {
    private String ime;
    private String prezime;
    public Reziser(){}
    public   Reziser(Parcel in){
        ime=in.readString();
        prezime=in.readString();


    }
    public static final Creator<Reziser> CREATOR=new Creator<Reziser>() {
        @Override
        public Reziser createFromParcel(Parcel source) {
            return new  Reziser(source);
        }

        @Override
        public Reziser[] newArray(int size) {
            return new Reziser[size];
        }
    };
    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }



    public Reziser(String ime, String prezime) {
        this.ime = ime;
        this.prezime = prezime;
    }
    @Override
    public int describeContents(){
        return  0;
    }
    @Override
    public void writeToParcel(Parcel dest,int flags){
        dest.writeString(ime);
        dest.writeString(prezime);


    }
}
