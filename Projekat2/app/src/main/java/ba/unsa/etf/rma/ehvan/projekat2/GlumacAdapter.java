package ba.unsa.etf.rma.ehvan.projekat2;

import android.content.Context;
import android.content.res.Resources;;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
/**
 * Created by lion on 4/12/17.
 */

public class GlumacAdapter extends ArrayAdapter<Glumac> {
    int res;

    public GlumacAdapter(@NonNull Context context, ArrayList<Glumac> m){
        super(context, R.layout.glumac, m);

    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater li = LayoutInflater.from(getContext());
        View temp = li.inflate(R.layout.glumac,parent,false);


        Glumac glumac = getItem(position);

        TextView tekst = (TextView) temp.findViewById(R.id.textView);
        TextView tekst2 = (TextView) temp.findViewById(R.id.textView2);
        TextView tekst3 = (TextView) temp.findViewById(R.id.textView3);
        TextView tekst4 = (TextView) temp.findViewById(R.id.textView4);
        ImageView slika = (ImageView) temp.findViewById(R.id.imageView);

        tekst.setText(glumac.getIme()+" "+glumac.getPrezime());
        tekst2.setText(glumac.getRodjenje());
        tekst3.setText(glumac.getMjesto());
        tekst4.setText(glumac.getRejting().toString());

        slika.setImageResource(glumac.getImage());

        return temp;
    }

}
