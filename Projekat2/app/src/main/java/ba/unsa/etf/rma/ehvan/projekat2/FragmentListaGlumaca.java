package ba.unsa.etf.rma.ehvan.projekat2;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by lion on 4/12/17.
 */

public class FragmentListaGlumaca extends Fragment {
    OnItemClick oic;
    private ArrayList<Glumac> glumci;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup containter, Bundle savedInstanceState){
        return inflater.inflate(R.layout.listaglumaca_fragment,containter,false);
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        if(getArguments().containsKey("Alista")){
            glumci=getArguments().getParcelableArrayList("Alista");
            ListView lv=(ListView)getView().findViewById(R.id.list);
            GlumacAdapter ga=new GlumacAdapter(getActivity(),glumci);
            lv.setAdapter(ga);

        try{
            oic=(OnItemClick)getActivity();
        }
        catch (ClassCastException e){
            throw  new ClassCastException(getActivity().toString()+"Treba implementirati OnItemClick");

        }
            lv.setOnItemClickListener(new AdapterView.OnItemClickListener(){
                @Override
                public void onItemClick(AdapterView<?> parent,View view,int position,long id){
                    oic.onItemClicked(position);
                }
            });
        }
    }
    public interface OnItemClick{
        public void onItemClicked(int pos);
    }
}
