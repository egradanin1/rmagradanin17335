package ba.unsa.etf.rma.ehvan.projekat2;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by lion on 4/12/17.
 */

public class Glumac  implements Parcelable{
    private String ime;
    private String prezime;
    private  String rodjenje;
    private String smrt;
    private String spol; //bio je boolean ali aplikacija je krahirala
    private String webStranica;
    private String biografija;
    private String mjesto;
    private Integer rejting;
    private  Integer image;
    public Glumac(){}
    public Glumac(String i, String p, String r,String s,String sp,String w, String b ,String m,Integer ra){
        ime=i;
        prezime=p;
        rodjenje=r;
        smrt=s;
        spol=sp;
        webStranica=w;
        biografija=b;
        mjesto=m;
        rejting=ra;
    }
    public  Glumac(Parcel in){
        ime=in.readString();
        prezime=in.readString();
        rodjenje=in.readString();
        smrt=in.readString();
        spol=in.readString();
        webStranica=in.readString();
        biografija=in.readString();
        mjesto=in.readString();
        rejting=in.readInt();
        image=in.readInt();

    }
    public static final Creator<Glumac> CREATOR=new Creator<Glumac>() {
        @Override
        public Glumac createFromParcel(Parcel source) {
            return new  Glumac(source);
        }

        @Override
        public Glumac[] newArray(int size) {
            return new Glumac[size];
        }
    };
    public void setImage(Integer i){image=i;}
    public  void setRejting(Integer ra){rejting=ra;}

    public void setIme(String i){
        ime=i;
    }

    public void setPrezime(String p){
        prezime=p;
    }

    public void setRodjenje(String r){rodjenje=r;}

    public void setSmrt(String s){smrt=s;}

    public void setSpol(String sp){spol=sp;}

    public void setWebStranica(String w){
        webStranica=w;
    }

    public void setBiografija(String b){
        biografija=b;
    }

    public void setMjesto(String m){mjesto=m;}

    public String getIme(){
        return ime;
    }

    public String getPrezime(){
        return prezime;
    }

    public String getRodjenje(){
        return rodjenje;
    }

    public String getSmrt(){return  smrt;}

    public String getSpol() { return spol;}

    public String getWebStranica(){
        return webStranica;
    }

    public String getBiografija(){
        return biografija;
    }

    public String getMjesto() {return  mjesto;}

    public Integer getRejting(){return  rejting;}

    public Integer getImage(){return  image;}
    @Override
    public int describeContents(){
        return  0;
    }
    @Override
    public void writeToParcel(Parcel dest,int flags){
        dest.writeString(ime);
        dest.writeString(prezime);
        dest.writeString(rodjenje);
        dest.writeString(smrt);
        dest.writeString(spol);
        dest.writeString(webStranica);
        dest.writeString( biografija);
        dest.writeString(mjesto);
        dest.writeInt(rejting);
        dest.writeInt(image);

    }
}
