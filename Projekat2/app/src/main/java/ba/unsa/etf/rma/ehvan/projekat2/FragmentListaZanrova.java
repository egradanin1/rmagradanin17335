package ba.unsa.etf.rma.ehvan.projekat2;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Created by lion on 4/12/17.
 */

public class FragmentListaZanrova extends Fragment{
    ArrayList<Zanr>zanrovi;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup containter, Bundle savedInstanceState){
        return inflater.inflate(R.layout.listazanrova_fragment,containter,false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getArguments().containsKey("Blista")) {
            zanrovi = getArguments().getParcelableArrayList("Blista");
            ListView lv = (ListView) getView().findViewById(R.id.zlist);
            ZanrAdapter za = new ZanrAdapter(getActivity(),zanrovi);
            lv.setAdapter(za);
        }
    }
}
