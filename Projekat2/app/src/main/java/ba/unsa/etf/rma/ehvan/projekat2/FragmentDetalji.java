package ba.unsa.etf.rma.ehvan.projekat2;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.view.View;


import static android.R.attr.fragment;

/**
 * Created by lion on 4/12/17.
 */

public class FragmentDetalji extends Fragment{
    private Glumac glumac;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup containter, Bundle savedInstanceState) {
        View iv = inflater.inflate(R.layout.detalji_fragment, containter, false);
        if(savedInstanceState!=null)
            glumac=savedInstanceState.getParcelable("parametri");
        if (getArguments() != null && getArguments().containsKey("glumac")) {
            glumac = getArguments().getParcelable("glumac");
            TextView imeiprezime = (TextView) iv.findViewById(R.id.atextView1);
            imeiprezime.setText(glumac.getIme() + " " + glumac.getPrezime());
            TextView rodjenje = (TextView) iv.findViewById(R.id.atextView2);
            rodjenje.setText("Rodjen-a:" + " " + glumac.getRodjenje());
            TextView smrt = (TextView) iv.findViewById(R.id.atextView3);
            if (glumac.getSmrt().equals("ziv"))
                smrt.setText("");
            else
                smrt.setText("Umro: " + glumac.getSmrt());
            TextView biografija = (TextView) iv.findViewById(R.id.atextView4);
            biografija.setText("O glumcu-ici: " + glumac.getBiografija() + " Rejting: " + glumac.getRejting().toString());
            TextView mjesto = (TextView) iv.findViewById(R.id.atextView5);
            mjesto.setText("Mjesto rodjenja: " + glumac.getMjesto());
            TextView spol = (TextView) iv.findViewById(R.id.atextView6);
            RelativeLayout l = (RelativeLayout) iv.findViewById(R.id.detalji);
            if (glumac.getSpol() == "Musko") {
                spol.setText("Spol: Musko");
                l.setBackgroundColor(Color.BLUE);


            } else {
                spol.setText("Spol: Zensko");
                l.setBackgroundColor(Color.RED);

            }

            TextView stranica = (TextView) iv.findViewById(R.id.atextView7);
            stranica.setText(glumac.getWebStranica());
            ImageView slika = (ImageView) iv.findViewById(R.id.aimageView);
            slika.setImageResource(glumac.getImage());
            stranica.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String url = glumac.getWebStranica();
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    if (i.resolveActivity(getActivity().getPackageManager()) != null) {
                        startActivity(i);
                    }
                }
            });
            Button dugme = (Button)iv.findViewById(R.id.dbutton);
            dugme.setOnClickListener(new View.OnClickListener()
            {
                public void onClick(View v)
                {

                    String textMessage= glumac.getBiografija();
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, textMessage);
                    sendIntent.setType("text/plain");
                    if (sendIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                        startActivity(sendIntent);
                    }
                }
            });


        }

        return  iv;

    }
    @Override
    public void onSaveInstanceState(Bundle b) {
        super.onSaveInstanceState(b);
        b.putParcelable("parametri", glumac);
    }

}
