package ba.unsa.etf.rma.ehvan.projekat2;
import android.content.Intent;
import android.view.View;
import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.FrameLayout;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements  FragmentListaGlumaca.OnItemClick,FragmentDugme.UpravljanjeFragmentima{
    final ArrayList<Glumac> unosi = new ArrayList<Glumac>();
    final ArrayList<Zanr> unosi1 = new ArrayList<Zanr>();
    final ArrayList<Reziser> unosi2 = new ArrayList<Reziser>();
    Boolean siriL;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        FragmentManager fm=getFragmentManager();
       // FragmentListaGlumaca f1=(FragmentListaGlumaca)fm.findFragmentByTag("Glumci");

            GlumacAdapter adapt = new GlumacAdapter(this, unosi);
            Glumac g = (new Glumac("enis", "Beslagic", "1975", "ziv", "Musko", "https://www.imdb.com/name/nm0078692/", "Podrijetlom je iz Tešnja. Diplomirao je na Akademiji scenskih umjetnosti u Sarajevu 2001. godine. Ostvario je uloge u više od 30 predstava. Najveću popularnost kod hrvatske publike stekao je nastupajući u seriji Naša mala klinika, te sa serijalom snimljenih viceva o Muji i Hasi.", "Tesanj", 5));
            g.setImage(R.drawable.enis);
            Glumac g1 = (new Glumac("angelina", "Jolie", "1975", "ziv", "Zensko", "http://www.imdb.com/name/nm0001401/?ref_=nv_sr_1", "Rođena je u Los Angelesu, u glumačkoj obitelji. Otac joj je glumac Jon Voight, poznat po filmovima Ponoćni kauboj (1969.) i Povratak veterana (1978.) za kojeg je dobio Oscara, a majka Marcheline Bertrand, bivša manekenka i glumica. Njeni roditelji su se razveli kada je Angelina imala godinu dana. ", "Los Angeles", 5));
            g1.setImage(R.drawable.angelina);

            unosi.add(g);
            unosi.add(g1);




            FragmentDugme fd = (FragmentDugme) fm.findFragmentById(R.id.mjestoF1);
            if (fd == null) {
                fd = new FragmentDugme();
                fm.beginTransaction().replace(R.id.mjestoF1, fd).commit();
            }

            FragmentListaGlumaca fg = new FragmentListaGlumaca();



            Bundle argumenti = new Bundle();
            argumenti.putParcelableArrayList("Alista",unosi);
            fg.setArguments(argumenti);

            fm.beginTransaction().replace(R.id.mjestoF2, fg).commit();















        ZanrAdapter za = new ZanrAdapter(this,unosi1);
        Zanr akcija = new Zanr("Akcija",R.drawable.action);
        unosi1.add(akcija);

        ReziserAdapter ra=new ReziserAdapter(this,unosi2);
        Reziser r1=new Reziser("Steven","Spielberg");
        unosi2.add(r1);







    }
    @Override
    public void onItemClicked(int pos){
        Bundle arguments=new Bundle();
        arguments.putParcelable("glumac",unosi.get(pos));
        FragmentDetalji fd=new FragmentDetalji();
        fd.setArguments(arguments);
        if(!getFragmentManager().beginTransaction().isEmpty())
                getFragmentManager().popBackStack();
        getFragmentManager().beginTransaction().replace(R.id.mjestoF2,fd).addToBackStack(null).commit();
    }
    @Override
    public void KlikniNaGlumca() {
        Bundle argumenti = new Bundle();
        argumenti.putParcelableArrayList("Alista",unosi);
        FragmentListaGlumaca fg = new FragmentListaGlumaca();
        fg.setArguments(argumenti);
        getFragmentManager().popBackStack();
        getFragmentManager().beginTransaction().replace(R.id.mjestoF2,fg).commit();
    }

    @Override
    public void KlikniNaZanr() {
        Bundle argumenti = new Bundle();
        argumenti.putParcelableArrayList("Blista",unosi1);
        FragmentListaZanrova fz = new FragmentListaZanrova();
        fz.setArguments(argumenti);
        getFragmentManager().popBackStack();
        getFragmentManager().beginTransaction().replace(R.id.mjestoF2,fz).commit();
    }

    @Override
    public void KlikniNaRezisera() {
        Bundle argumenti = new Bundle();
        argumenti.putParcelableArrayList("Clista",unosi2);
        FragmentListaRezisera fr = new FragmentListaRezisera();
        fr.setArguments(argumenti);
        getFragmentManager().popBackStack();
        getFragmentManager().beginTransaction().replace(R.id.mjestoF2,fr).commit();
    }



}
