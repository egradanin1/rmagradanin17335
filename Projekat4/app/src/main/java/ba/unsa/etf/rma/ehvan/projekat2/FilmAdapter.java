package ba.unsa.etf.rma.ehvan.projekat2;



import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class FilmAdapter extends ArrayAdapter<Film>{
    int res;
    public FilmAdapter(@NonNull Context context, ArrayList<Film> m){
        super(context, R.layout.film, m);

    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater li = LayoutInflater.from(getContext());
        View temp = li.inflate(R.layout.film,parent,false);


        Film  film = getItem(position);

        TextView tekst = (TextView) temp.findViewById(R.id.ftextView);



        tekst.setText(film.getNaziv());



        return temp;
    }
}
