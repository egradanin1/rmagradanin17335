package ba.unsa.etf.rma.ehvan.projekat2;

import android.app.Fragment;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by lion on 4/12/17.
 */

public class FragmentListaZanrova extends Fragment implements CustomResultReceiver.Receiver{
    ArrayList<Zanr>zanrovi;
    int id;
    public CustomResultReceiver.Receiver funkcija(){
        return FragmentListaZanrova.this;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup containter, Bundle savedInstanceState){
        return inflater.inflate(R.layout.listazanrova_fragment,containter,false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        boolean baza=false;
        int glumciID=0;
        GlumacDBOpenHelper pom = new GlumacDBOpenHelper(getActivity(), "tabela_glumaca", null, 1);
        SQLiteDatabase db1 = pom.getWritableDatabase();
        zanrovi=new ArrayList<>();
        String[] koloneRezulat1 = new String[]{GlumacDBOpenHelper.GLUMAC_ID, GlumacDBOpenHelper.GLUMAC_IMEIPREZIME, GlumacDBOpenHelper.GLUMAC_RODJENJE,
                GlumacDBOpenHelper.GLUMAC_SMRT, GlumacDBOpenHelper.GLUMAC_SPOL, GlumacDBOpenHelper.GLUMAC_WEB,
                GlumacDBOpenHelper.GLUMAC_BIOGRAFIJA, GlumacDBOpenHelper.GLUMAC_MJESTO, GlumacDBOpenHelper.GLUMAC_REJTING,
                GlumacDBOpenHelper.GLUMAC_IMAGE};
        Cursor cursorg = db1.query(GlumacDBOpenHelper.DATABASE_TABLE1,
                koloneRezulat1, null,
                null, null, null, null);
        if (cursorg.moveToFirst()) {
            do {



                if (cursorg.getString(1).equals(getArguments().getString("imeglumca"))) {
                    glumciID=cursorg.getInt(0);
                    baza=true;

                }
                //Glumac g = new Glumac(imeiprezime, rodjenje, smrt, spol, webStranica, biografija, mjesto, rejting);
            }
            while (cursorg.moveToNext());
        }
        if (cursorg != null && !cursorg.isClosed()) {
            cursorg.close();
        }
        String[] koloneRezulat = new String[]{GlumacDBOpenHelper.ZANR_ID, GlumacDBOpenHelper.ZANR_NAZIV};
        Cursor cursor=db1.query(GlumacDBOpenHelper.DATABASE_TABLE3, koloneRezulat, null, null, null, null, null);
        String[] kolone = new String[]{GlumacDBOpenHelper.ZG_ID, GlumacDBOpenHelper.ZG_GLUMACID,GlumacDBOpenHelper.ZG_ZANRID};
        Cursor c=db1.query(GlumacDBOpenHelper.DATABASE_TABLE5, kolone, null, null, null, null, null);


        ArrayList<Integer> ZanrID=new ArrayList<>();
        if (c.moveToFirst()) {
            do {



                if (c.getInt(1)==glumciID) {
                    ZanrID.add(c.getInt(2));

                }
                //Glumac g = new Glumac(imeiprezime, rodjenje, smrt, spol, webStranica, biografija, mjesto, rejting);
            }
            while (c.moveToNext());
        }
        if (c != null && !c.isClosed()) {
            c.close();
        }
        if (cursor.moveToFirst()) {
            do {



                if (ZanrID.contains(cursor.getInt(0))) {

                    zanrovi.add(new Zanr(cursor.getString(1)));
                }
                //Glumac g = new Glumac(imeiprezime, rodjenje, smrt, spol, webStranica, biografija, mjesto, rejting);
            }
            while (cursor.moveToNext());
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }if(!zanrovi.isEmpty()){
            ListView lv1 = (ListView) getView().findViewById(R.id.zlist);
            ZanrAdapter ra1 = new ZanrAdapter(getActivity(), zanrovi);
            lv1.setAdapter(ra1);}
        pom.close();
       if (getArguments().containsKey("Blista") && baza==false) {
            zanrovi = getArguments().getParcelableArrayList("Blista");
            ListView lv = (ListView) getView().findViewById(R.id.zlist);
            ZanrAdapter za = new ZanrAdapter(getActivity(),zanrovi);
            lv.setAdapter(za);
        }
        if(baza==false){
        zanrovi.removeAll(zanrovi);
        //new SearchGenres((SearchGenres.onGenresSearchDone) FragmentListaZanrova.this).execute(getArguments().getString("id"));
        Intent intent = new Intent(Intent.ACTION_SYNC, null, getActivity(), GenresIntentService.class);
        CustomResultReceiver mReceiver = new CustomResultReceiver(new Handler());
        mReceiver.setReceiver(funkcija());
        intent.putExtra("ime",getArguments().getString("id"));
        intent.putExtra("receiver", mReceiver);
        getActivity().startService(intent);}
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {


        switch (resultCode) {

            case GenresIntentService.STATUS_RUNNING:

                Toast.makeText(getActivity(), "Pretrazivanje u toku", Toast.LENGTH_LONG).show();
                break;
            case GenresIntentService.STATUS_FINISHED:

                ArrayList<Zanr> rez = resultData.getParcelableArrayList("result");
                for (Zanr g:rez
                        ) {
                    zanrovi.add(g);
                }
                ListView lv=(ListView)getView().findViewById(R.id.zlist);
                ZanrAdapter za=new ZanrAdapter(getActivity(),zanrovi);
                lv.setAdapter(za);
                Toast.makeText(getActivity(), "Pretrazivanje gotovo", Toast.LENGTH_LONG).show();
                break;
            case GenresIntentService.STATUS_ERROR:

                String error = resultData.getString(Intent.EXTRA_TEXT);
                Toast.makeText(getActivity(), "Doslo je do greske!", Toast.LENGTH_LONG).show();
                break;
        }

    }
}
