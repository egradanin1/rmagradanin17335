package ba.unsa.etf.rma.ehvan.projekat2;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;
import android.os.Handler;



public class FragmentListaFilmova extends Fragment implements  CustomResultReceiver.Receiver{
    OnItemClick1 oic;
    private ArrayList<Film> filmovi;

    public CustomResultReceiver.Receiver funkcija(){
        return FragmentListaFilmova.this;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup containter, Bundle savedInstanceState){
        return inflater.inflate(R.layout.listafilmova_fragment,containter,false);
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        Button dugme=(Button) getView().findViewById(R.id.PretraziFilmove);
        final EditText tekst=(EditText)getView().findViewById(R.id.pretragafilmova);
        dugme.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                Log.i("message","uslo je ovdje");
                Intent intent = new Intent(Intent.ACTION_SYNC, null, getActivity(), MoviesIntentService.class);
                CustomResultReceiver mReceiver = new CustomResultReceiver(new Handler());
                mReceiver.setReceiver(funkcija());
                intent.putExtra("ime",tekst.getText().toString());
                intent.putExtra("receiver", mReceiver);
                getActivity().startService(intent);
                //new SearchActors((SearchActors.onActorSearchDone) FragmentListaGlumaca.this).execute(tekst.getText().toString());
            }
        });

        if(getArguments().containsKey("Dlista")){
            filmovi=getArguments().getParcelableArrayList("Dlista");
            ListView lv=(ListView)getView().findViewById(R.id.listfilmova);
            FilmAdapter ga=new FilmAdapter(getActivity(),filmovi);
            lv.setAdapter(ga);


            try{
                oic=(OnItemClick1)getActivity();
            }
            catch (ClassCastException e){
                throw  new ClassCastException(getActivity().toString()+"Treba implementirati OnItemClick");

            }
            lv.setOnItemClickListener(new AdapterView.OnItemClickListener(){
                @Override
                public void onItemClick(AdapterView<?> parent,View view,int position,long id){
                    oic.onItemClicked1(position);
                }
            });
        }

    }
    public interface OnItemClick1{
        public void onItemClicked1(int pos);
    }


    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {


        switch (resultCode) {


            case MoviesIntentService.STATUS_RUNNING:

                Toast.makeText(getActivity(), "Pretrazivanje u toku", Toast.LENGTH_LONG).show();
                break;
            case MoviesIntentService.STATUS_FINISHED:
                //glumci.removeAll(glumci);
                ArrayList<Film> rez = resultData.getParcelableArrayList("result");

                for (Film g:rez
                        ) {
                    filmovi.add(g);
                }
                ListView lv=(ListView)getView().findViewById(R.id.listfilmova);
                FilmAdapter ga=new FilmAdapter(getActivity(),filmovi);
                lv.setAdapter(ga);
                Toast.makeText(getActivity(), "Pretrazivanje gotovo", Toast.LENGTH_LONG).show();
                break;
            case MoviesIntentService.STATUS_ERROR:

                String error = resultData.getString(Intent.EXTRA_TEXT);
                Toast.makeText(getActivity(), "Doslo je do greske!", Toast.LENGTH_LONG).show();
                break;
        }

    }
}