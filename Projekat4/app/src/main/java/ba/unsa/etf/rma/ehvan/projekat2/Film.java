package ba.unsa.etf.rma.ehvan.projekat2;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by lion on 4/12/17.
 */

public class Film implements Parcelable {
    private String naziv;


    public  Film(Parcel in){
        naziv=in.readString();



    }

    public static final Creator<Film> CREATOR=new Creator<Film>() {
        @Override
        public Film createFromParcel(Parcel source) {
            return new  Film(source);
        }

        @Override
        public Film[] newArray(int size) {
            return new Film[size];
        }
    };
    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }







    public Film(String naziv) {
        this.naziv = naziv;

    }
    @Override
    public int describeContents(){
        return  0;
    }
    @Override
    public void writeToParcel(Parcel dest,int flags){
        dest.writeString(naziv);



    }
}
