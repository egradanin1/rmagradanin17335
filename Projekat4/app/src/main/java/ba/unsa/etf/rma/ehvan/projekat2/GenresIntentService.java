package ba.unsa.etf.rma.ehvan.projekat2;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by lion on 5/15/17.
 */

public class GenresIntentService extends IntentService {
    public static final int STATUS_RUNNING = 0;
    public static final int STATUS_FINISHED = 1;
    public static final int STATUS_ERROR = 2;

    public GenresIntentService() {
        super(null);
    }

    public GenresIntentService(String name) {
        super(name);

    }

    @Override
    public void onCreate() {
        super.onCreate();

    }

    ArrayList<Zanr> rez=new ArrayList<>();
    @Override
    protected void onHandleIntent(Intent intent) {

        ArrayList<String> lista=new ArrayList<String>();
        ArrayList<String>bezduplikata=new ArrayList<>() ;
        ArrayList<String>zanrovi=new ArrayList<>() ;
        final ResultReceiver receiver = intent.getParcelableExtra("receiver");
        Bundle bundle = new Bundle();

        /* Update UI: Početak taska */
        receiver.send(STATUS_RUNNING, Bundle.EMPTY);


        String query = null;
        query=intent.getStringExtra("ime");

        String url1="https://api.themoviedb.org/3/discover/movie?api_key=bcf63027b3451894a3574dbb2b436f82&with_cast="+query+"&primary_release_date.lte=2017-5-26&sort_by=release_date.desc";

        try{

            URL url = new URL(url1);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in=null;



            try {

                in = new BufferedInputStream(urlConnection.getInputStream());

            }
            catch (Exception e){
                Log.i("exception",e.getMessage().toString());
            }

            String rezultat=convertStreamToString(in);
            JSONObject jo=new JSONObject(rezultat );
            JSONArray results=jo.getJSONArray("results");

            int velicina;
            if(results.length()>6)
                velicina=7;
            else
                velicina=results.length();
            for(int i=0;i<velicina;i++){
                Log.i("Ovdje pada","Jbg");
                JSONObject genres=results.getJSONObject(i);
                JSONArray ids=genres.getJSONArray("genre_ids");
                if(ids.length()!=0)
                    lista.add(ids.getString(0));


            }


            for (String s :lista
                    ) {
                if(!bezduplikata.contains(s))
                    bezduplikata.add(s);

            }
            for (String s:bezduplikata
                    ) {
                Log.i("Super",s);
            }

        }
        catch (MalformedURLException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        String url2="https://api.themoviedb.org/3/genre/movie/list?api_key=bcf63027b3451894a3574dbb2b436f82&language=en-US";

        try{

            URL url = new URL(url2);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in=null;



            try {

                in = new BufferedInputStream(urlConnection.getInputStream());

            }
            catch (Exception e){
                Log.i("exception",e.getMessage().toString());
            }
            String rezultat=convertStreamToString(in);
            JSONObject jo=new JSONObject(rezultat );
            JSONArray genres=jo.getJSONArray("genres");
            for (String s:bezduplikata
                    ) {


                for (int i = 0; i < genres.length(); i++) {
                    JSONObject item = genres.getJSONObject(i);
                    if(item.getString("id").equals(s))
                        zanrovi.add(item.getString("name"));

                }
            }



            for (String s : zanrovi
                    ) {
                rez.add(new Zanr(s));

            }

        }
        catch (MalformedURLException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        bundle.putParcelableArrayList("result", rez);
        receiver.send(STATUS_FINISHED, bundle);

    }

    public String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
        } finally {
            try {
                is.close();
            } catch (IOException e) {
            }
        }
        return sb.toString();
    }
}

