package ba.unsa.etf.rma.ehvan.projekat2;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by lion on 4/12/17.
 */

public class Reziser implements Parcelable {
    private String imeiprezime;

    public   Reziser(Parcel in){
        imeiprezime=in.readString();



    }
    public static final Creator<Reziser> CREATOR=new Creator<Reziser>() {
        @Override
        public Reziser createFromParcel(Parcel source) {
            return new  Reziser(source);
        }

        @Override
        public Reziser[] newArray(int size) {
            return new Reziser[size];
        }
    };
    public String getImeiPrezime() {
        return imeiprezime;
    }

    public void setImeiPrezime(String ime) {
        this.imeiprezime = ime;
    }





    public Reziser(String ime) {
        this.imeiprezime = ime;

    }
    @Override
    public int describeContents(){
        return  0;
    }
    @Override
    public void writeToParcel(Parcel dest,int flags){
        dest.writeString(imeiprezime);



    }
    @Override
    public String toString() {
        return imeiprezime ;
    }

}
