package ba.unsa.etf.rma.ehvan.projekat2;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;



public class MoviesIntentService extends IntentService {

    public static final int STATUS_RUNNING = 0;
    public static final int STATUS_FINISHED = 1;
    public static final int STATUS_ERROR = 2;

    public MoviesIntentService() {
        super(null);
    }

    public MoviesIntentService(String name) {
        super(name);

    }

    @Override
    public void onCreate() {
        Log.i("messaege","uslo je !!!");
        super.onCreate();

    }

    ArrayList<Film> rez=new ArrayList<>();
    @Override
    protected void onHandleIntent(Intent intent) {


        final ResultReceiver receiver = intent.getParcelableExtra("receiver");
        Bundle bundle = new Bundle();

        /* Update UI: Početak taska */
        receiver.send(STATUS_RUNNING, Bundle.EMPTY);


        String query = null;
        query=intent.getStringExtra("ime");
        JSONArray results=null;
        int velicina=0;


        String url1="https://api.themoviedb.org/3/search/movie?api_key=bcf63027b3451894a3574dbb2b436f82&query="+query;

        try{

            URL url = new URL(url1);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in=null;



            try {

                in = new BufferedInputStream(urlConnection.getInputStream());

            }
            catch (Exception e){
                Log.i("exception",e.getMessage().toString());
            }
            String rezultat=convertStreamToString(in);

            JSONObject jo=new JSONObject(rezultat );
            results=jo.getJSONArray("results");
            if(results.length()<7)
                velicina=results.length();
            else
                velicina=7;
            //stavljeno radi testiranja
            //Log.i("ID",id);
            Log.i("velicina",String.valueOf(velicina));

        }
        catch (MalformedURLException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        for(int j=0;j<velicina;j++) {


            try {
                Log.i("message","uslo je ovdje");
                JSONObject film = results.getJSONObject(j);
                Film f=new Film(film.getString("title"));
                rez.add(f);


            }
            catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.getMessage().toString();
            }


        }
        bundle.putParcelableArrayList("result", rez);
        receiver.send(STATUS_FINISHED, bundle);
    }

    public String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
        } finally {
            try {
                is.close();
            } catch (IOException e) {
            }
        }
        return sb.toString();
    }
}
