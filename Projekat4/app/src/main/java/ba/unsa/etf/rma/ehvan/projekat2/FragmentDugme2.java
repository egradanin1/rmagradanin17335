package ba.unsa.etf.rma.ehvan.projekat2;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * Created by lion on 5/10/17.
 */

public class FragmentDugme2 extends Fragment {


    FragmentDugme2.UpravljanjeFragmentima2 uf2;

    public interface UpravljanjeFragmentima2 {
        public void KlikniNaGlumce();
        public void KlikniNaOstalo();
       // public void KlikniNaFilmove();
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            uf2 = (FragmentDugme2.UpravljanjeFragmentima2)activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup containter, Bundle savedInstanceState){
        View pogled = inflater.inflate(R.layout.dugme2_fragment,containter,false);
        Button g = (Button)pogled.findViewById(R.id.dugme_glumci);
        Button o= (Button)pogled.findViewById(R.id.dugme_ostalo);
        //Button f=(Button)pogled.findViewById(R.id.dugme_filmovi);
        g.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View v) {
                        uf2.KlikniNaGlumce();
                    }
                }

        );

        o.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View v) {
                        uf2.KlikniNaOstalo();
                    }
                }

        );
       /* f.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View v) {
                        uf2.KlikniNaFilmove();
                    }
                }

        );*/



        return pogled;
    }
}
