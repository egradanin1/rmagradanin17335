package ba.unsa.etf.rma.ehvan.projekat2;

import android.content.Context;
import android.content.res.Resources;;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
/**
 * Created by lion on 4/12/17.
 */

public class GlumacAdapter extends ArrayAdapter<Glumac> {
    int res;

    public GlumacAdapter(Context context, ArrayList<Glumac> m){
        super(context, R.layout.glumac, m);

    }


    @Override
    public View getView(int position,  View convertView,  ViewGroup parent) {
        LayoutInflater li = LayoutInflater.from(getContext());
        View temp = li.inflate(R.layout.glumac,parent,false);


        Glumac glumac = getItem(position);

        TextView tekst = (TextView) temp.findViewById(R.id.textView);
        TextView tekst2 = (TextView) temp.findViewById(R.id.textView2);
        TextView tekst3 = (TextView) temp.findViewById(R.id.textView3);
        TextView tekst4 = (TextView) temp.findViewById(R.id.textView4);
        ImageView slika = (ImageView) temp.findViewById(R.id.gimageView);

        tekst.setText(glumac.getImeiPrezime());
        tekst2.setText(glumac.getRodjenje());
        tekst3.setText(glumac.getMjesto());
        tekst4.setText(glumac.getRejting());
        Picasso.with(getContext())
                .load(glumac.getImage())
                .resize(50, 50)
                .centerCrop()
                .into(slika);

        //slika.setImageResource(glumac.getImage());

        return temp;
    }

}
