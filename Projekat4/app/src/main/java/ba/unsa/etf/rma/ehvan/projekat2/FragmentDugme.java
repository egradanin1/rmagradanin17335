package ba.unsa.etf.rma.ehvan.projekat2;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import java.util.Locale;

/**
 * Created by lion on 4/12/17.
 */

public class FragmentDugme extends Fragment {
    UpravljanjeFragmentima uF;

    public interface UpravljanjeFragmentima {
        public void KlikniNaGlumca();
        public void KlikniNaRezisera();
        public void KlikniNaZanr();
        public void KlikniNaFilm();
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            uF = (UpravljanjeFragmentima) activity;
        }
        catch (ClassCastException e) {
            throw new ClassCastException(activity.toString());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup containter, Bundle savedInstanceState){
        View iv= inflater.inflate(R.layout.dugme_fragment,containter,false);
         Button glumac = (Button)iv.findViewById(R.id.button);
         Button reziser= (Button)iv.findViewById(R.id.button1);
         Button zanr= (Button)iv.findViewById(R.id.button2);
        Button film=(Button) iv.findViewById(R.id.button5);
        ImageView slika=(ImageView)iv.findViewById(R.id.imageView) ;
        glumac.setText(R.string.Glumci);
        reziser.setText(R.string.Reziseri);
        zanr.setText(R.string.Zanrovi);
        if(Locale.getDefault().toString().contains("en"))
            slika.setImageResource(R.drawable.uk);
        else
            slika.setImageResource(R.drawable.bih);

        glumac.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View v) {
                        uF.KlikniNaGlumca();
                    }
                }

        );

        reziser.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View v) {
                        uF.KlikniNaRezisera();
                    }
                }

        );

        zanr.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View v) {
                        uF.KlikniNaZanr();
                    }
                }

        );
        film.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View v) {
                        uF.KlikniNaFilm();
                    }
                }

        );
        return  iv;

    }
}
