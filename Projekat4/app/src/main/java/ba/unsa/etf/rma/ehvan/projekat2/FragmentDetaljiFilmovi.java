package ba.unsa.etf.rma.ehvan.projekat2;

import android.app.Fragment;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.view.View;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.net.Uri;
import android.provider.CalendarContract;

import java.util.Calendar;
import java.util.TimeZone;


import com.squareup.picasso.Picasso;

import static android.R.attr.fragment;


public class FragmentDetaljiFilmovi extends Fragment{
    private Film film;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup containter, Bundle savedInstanceState) {
        View iv = inflater.inflate(R.layout.detalji_filmova, containter, false);
        if(savedInstanceState!=null)
            film=savedInstanceState.getParcelable("param");
        if (getArguments() != null && getArguments().containsKey("film")) {
            film= getArguments().getParcelable("film");
           final TextView naziv = (TextView) iv.findViewById(R.id.textView6);
            naziv.setText(film.getNaziv());
            Button dugme = (Button)iv.findViewById(R.id.button3);
            final EditText opis=(EditText)iv.findViewById(R.id.pretragafilmova);
            final EditText datum=(EditText)iv.findViewById(R.id.editText3);
            dugme.setOnClickListener(new View.OnClickListener()
            {
                public void onClick(View v)
                {
                    Calendar cal = Calendar.getInstance();
                    Intent intent = new Intent(Intent.ACTION_EDIT);
                    intent.setType("vnd.android.cursor.item/event");
                    intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME,datum.getText().toString());
                    intent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME,cal.getTimeInMillis()+60*60*1000);
                    intent.putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, true);
                    intent.putExtra(CalendarContract.Events.TITLE, naziv.getText().toString());
                    intent.putExtra(CalendarContract.Events.DESCRIPTION, opis.getText().toString());
                    intent.putExtra(CalendarContract.Events.EVENT_LOCATION, "Sarajevo");
                    intent.putExtra(CalendarContract.Events.RRULE, "FREQ=YEARLY");
                    startActivity(intent);


                }
            });


        }

        return  iv;

    }
    @Override
    public void onSaveInstanceState(Bundle b) {
        super.onSaveInstanceState(b);
        b.putParcelable("param", film);
    }

}