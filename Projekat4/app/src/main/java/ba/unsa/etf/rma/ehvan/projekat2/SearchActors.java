package ba.unsa.etf.rma.ehvan.projekat2;

/**
 * Created by lion on 5/14/17.
 */


import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Scanner;

import javax.net.ssl.HttpsURLConnection;

import static android.webkit.ConsoleMessage.MessageLevel.LOG;

/**
 * Created by lion on 5/8/17.
 */

public class SearchActors extends AsyncTask<String,Integer,Void> {
    public interface onActorSearchDone{
        public void onDone(ArrayList<Glumac> rez);
    }
    ArrayList<Glumac> rez=new ArrayList<>();
    private onActorSearchDone pozivatelj;
    public SearchActors(onActorSearchDone p){pozivatelj=p;}
    @Override
    protected Void doInBackground(String... params) {
        String query=null;
        try{
            query= URLEncoder.encode(params[0],"utf-8");


        }
        catch (UnsupportedEncodingException e){
            e.printStackTrace();

        }
        String imeiprezime;
        String rodjenje;
        String smrt;
        String spol;
        String webStranica;
        String biografija;
        String mjesto;
        String rejting;
        String image=null;
        String id=null;
        String url1="https://api.themoviedb.org/3/search/person?api_key=bcf63027b3451894a3574dbb2b436f82&query="+query;

        try{

            URL url = new URL(url1);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in=null;



            try {

                in = new BufferedInputStream(urlConnection.getInputStream());

            }
            catch (Exception e){
                Log.i("exception",e.getMessage().toString());
            }
            String rezultat=convertStreamToString(in);

            JSONObject jo=new JSONObject(rezultat );
            JSONArray results=jo.getJSONArray("results");
            JSONObject actor=results.getJSONObject(0);
            id=actor.getString("id");
            Log.i("ID",id);

        }
        catch (MalformedURLException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        String url2="https://api.themoviedb.org/3/person/"+id+"?api_key=bcf63027b3451894a3574dbb2b436f82&language=en-US";

        try{

            URL url = new URL(url2);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in=null;



            try {

                in = new BufferedInputStream(urlConnection.getInputStream());

            }
            catch (Exception e){
                Log.i("exception",e.getMessage().toString());
            }
            String rezultat=convertStreamToString(in);

            JSONObject jo=new JSONObject(rezultat );


            imeiprezime=jo.getString("name");
            rodjenje=jo.getString("birthday");
            smrt=jo.getString("deathday");
            if(smrt.isEmpty())
                smrt="ziv";
            if(jo.getString("gender").equals("2"))
                spol="Musko";
            else
                spol="Zensko";
            webStranica="http://www.imdb.com/name/"+jo.getString("imdb_id")+"/";

            biografija=jo.getString("biography");
            int i=biografija.indexOf(".");
            biografija=biografija.substring(0,i)+".";
            mjesto=jo.getString("place_of_birth").substring(0,jo.getString("place_of_birth").indexOf(","));
            rejting=jo.getString("popularity");
            image="https://image.tmdb.org/t/p/w640"+jo.getString("profile_path");
            Glumac g=new Glumac(imeiprezime,rodjenje,smrt,spol,webStranica,biografija,mjesto,rejting);
            g.setImage(image);
            g.setID(id);
            rez.add(g);

            //Log.i("detalji",imeiprezime+" "+ rodjenje+ " " + smrt + " "+ spol+ " "+ webStranica+" "+ biografija+" "+mjesto+" "+rejting+" "+image);

        }
        catch (MalformedURLException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        catch (Exception e){
            e.getMessage().toString();
        }



        return null;
    }
    public String convertStreamToString(InputStream is){
        BufferedReader reader=new BufferedReader(new InputStreamReader(is));
        StringBuilder sb=new StringBuilder();
        String line=null;
        try{
            while((line=reader.readLine())!=null) {
                sb.append(line+"\n");
            }
        }
        catch (IOException e){}
        finally {
            try{
                is.close();
            }
            catch (IOException e){}
        }
        return  sb.toString();
    }


    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        pozivatelj.onDone(rez);
    }
}

