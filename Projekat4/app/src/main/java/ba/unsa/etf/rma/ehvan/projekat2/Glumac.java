package ba.unsa.etf.rma.ehvan.projekat2;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by lion on 4/12/17.
 */

public class Glumac  implements Parcelable{
    private String imeiprezime;

    private  String rodjenje;
    private String smrt;
    private String spol; //bio je boolean ali aplikacija je krahirala
    private String webStranica;
    private String biografija;
    private String mjesto;
    private String rejting;
    private String image;
    private String id;

    public Glumac(String i, String r,String s,String sp,String w, String b ,String m,String ra){
        imeiprezime=i;
        rodjenje=r;
        smrt=s;
        spol=sp;
        webStranica=w;
        biografija=b;
        mjesto=m;
        rejting=ra;
    }
    public  Glumac(Parcel in){
        imeiprezime=in.readString();
        rodjenje=in.readString();
        smrt=in.readString();
        spol=in.readString();
        webStranica=in.readString();
        biografija=in.readString();
        mjesto=in.readString();
        rejting=in.readString();
        image=in.readString();
        id=in.readString();

    }
    public static final Creator<Glumac> CREATOR=new Creator<Glumac>() {
        @Override
        public Glumac createFromParcel(Parcel source) {
            return new  Glumac(source);
        }

        @Override
        public Glumac[] newArray(int size) {
            return new Glumac[size];
        }
    };
    public void setImage(String i){image=i;}
    public  void setRejting(String ra){rejting=ra;}

    public void setImeiPrezime(String i){
        imeiprezime=i;
    }



    public void setRodjenje(String r){rodjenje=r;}
    public void setID(String i){id=i;}

    public void setSmrt(String s){smrt=s;}

    public void setSpol(String sp){spol=sp;}

    public void setWebStranica(String w){
        webStranica=w;
    }

    public void setBiografija(String b){
        biografija=b;
    }

    public void setMjesto(String m){mjesto=m;}

    public String getImeiPrezime(){
        return imeiprezime;
    }


    public String getID(){return  id;}
    public String getRodjenje(){
        return rodjenje;
    }

    public String getSmrt(){return  smrt;}

    public String getSpol() { return spol;}

    public String getWebStranica(){
        return webStranica;
    }

    public String getBiografija(){
        return biografija;
    }

    public String getMjesto() {return  mjesto;}

    public String getRejting(){return  rejting;}

    public String getImage(){return  image;}
    @Override
    public int describeContents(){
        return  0;
    }
    @Override
    public void writeToParcel(Parcel dest,int flags){
        dest.writeString(imeiprezime);
        dest.writeString(rodjenje);
        dest.writeString(smrt);
        dest.writeString(spol);
        dest.writeString(webStranica);
        dest.writeString( biografija);
        dest.writeString(mjesto);
        dest.writeString(rejting);
        dest.writeString(image);

    }
    @Override
    public String toString() {
        return imeiprezime + '\n' + rodjenje + '\n' + mjesto + "     " + rejting;
    }

}
