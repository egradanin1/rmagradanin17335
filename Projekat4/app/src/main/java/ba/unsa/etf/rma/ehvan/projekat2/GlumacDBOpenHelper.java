package ba.unsa.etf.rma.ehvan.projekat2;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

/**
 * Created by Korisnik on 5.6.2016.
 */
public class GlumacDBOpenHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "mojaBaza.db";
    public static final String DATABASE_TABLE1 = "Glumci";
    public static final String DATABASE_TABLE2 = "Reziseri";
    public static final String DATABASE_TABLE3 = "Zanrovi";
    public static final String DATABASE_TABLE4 = "glumci_reziseri";
    public static final String DATABASE_TABLE5 = "glumci_zanrovi";
    public static final int DATABASE_VERSION = 1;
    public static final String GLUMAC_ID ="glumacID";
    public static final String GLUMAC_IMEIPREZIME ="imeiprezime";
    public static final String GLUMAC_SMRT ="smrt";
    public static final String GLUMAC_RODJENJE ="rodjenje";
    public static final String GLUMAC_SPOL ="spol";
    public static final String GLUMAC_WEB ="web";
    public static final String GLUMAC_BIOGRAFIJA ="biografija";
    public static final String GLUMAC_MJESTO ="mjesto";
    public static final String GLUMAC_REJTING ="rejting";
    public static final String GLUMAC_IMAGE ="slika";
    public static final String REZISER_ID="reziserID";
    public static final String REZISER_IMEIPREZIME ="r_imeiprezime";
    public static final String ZANR_ID="zanrID";
    public static final String ZANR_NAZIV="naziv";
    public static final String RG_ID="glumac_reziser_odnosID";
    public static final String RG_GLUMACID="glumac_id";
    public static final String RG_REZISERID="reziser_id";
    public static final String ZG_ID="glumac_zanr_odnosID";
    public static final String ZG_GLUMACID="glumac_zid";
    public static final String ZG_ZANRID="zanr_id";




//  kreiranje baza

    //TABELA GLUMCI
    private static final String DATABASE_CREATE1 = "create table " +
            DATABASE_TABLE1 + " (" + GLUMAC_ID +
            " integer primary key autoincrement, " +
            GLUMAC_IMEIPREZIME + " text not null, " + GLUMAC_RODJENJE + " text not null, " +
            GLUMAC_SMRT + " text not null, "+  GLUMAC_SPOL + " text not null, "+  GLUMAC_WEB + " text not null, "
            + GLUMAC_BIOGRAFIJA + " text not null, "+ GLUMAC_MJESTO + " text not null, "+
            GLUMAC_REJTING + " text not null, "+ GLUMAC_IMAGE + " text not null);";
    //TABELA REZISERI
    private static final String DATABASE_CREATE2 = "create table " +
            DATABASE_TABLE2 + " (" + REZISER_ID +
            " integer primary key autoincrement, " + REZISER_IMEIPREZIME +" text not null);";
    //TABELA ZANROVI
    private static final String DATABASE_CREATE3 = "create table " +
            DATABASE_TABLE3 + " (" + ZANR_ID +
            " integer primary key autoincrement, " +
             ZANR_NAZIV +" text not null);";
    //TABELA ODNOS GLUMCI I REZISERI
    private static final String DATABASE_CREATE4= "create table " +
            DATABASE_TABLE4 + " (" + RG_ID +
            " integer primary key autoincrement, " + RG_GLUMACID + " integer not null, "+
            RG_REZISERID +" integer not null);";
    //TABELA ODNOS GLUMCI I ZANROVI
    private static final String DATABASE_CREATE5= "create table " +
            DATABASE_TABLE5 + " (" + ZG_ID +
            " integer primary key autoincrement, " + ZG_GLUMACID + " integer not null, "+
            ZG_ZANRID +" integer not null);";

    public GlumacDBOpenHelper(Context context, String name,
                               SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DATABASE_CREATE1);
        db.execSQL(DATABASE_CREATE2);
        db.execSQL(DATABASE_CREATE3);
        db.execSQL(DATABASE_CREATE4);
        db.execSQL(DATABASE_CREATE5);
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF IT EXISTS" + DATABASE_TABLE1);
        db.execSQL("DROP TABLE IF IT EXISTS" + DATABASE_TABLE2);
        db.execSQL("DROP TABLE IF IT EXISTS" + DATABASE_TABLE3);
        db.execSQL("DROP TABLE IF IT EXISTS" + DATABASE_TABLE4);
        db.execSQL("DROP TABLE IF IT EXISTS" + DATABASE_TABLE5);
        onCreate(db);
    }


    public boolean insertReziser(String imeiprezime){
        boolean postoji=false;
        SQLiteDatabase db = this.getWritableDatabase();
        String[] koloneRezulat = new String[]{ GlumacDBOpenHelper.REZISER_ID, GlumacDBOpenHelper.REZISER_IMEIPREZIME};
        Cursor cursor = db.query(GlumacDBOpenHelper.DATABASE_TABLE2,
                koloneRezulat, null,
                null, null, null, null);

        if(cursor.moveToFirst()){
            do{
                if(cursor.getString(1).equals(imeiprezime) ){
                    postoji=true;
                }
            }
            while(cursor.moveToNext());
        }
        if (cursor != null && !cursor.isClosed())
        {
            cursor.close();
        }
        if(postoji)
            return false;


        db.execSQL("INSERT INTO "+DATABASE_TABLE2+" ("+REZISER_IMEIPREZIME+") VALUES ('"+imeiprezime+"');");
        return true;
    }
    public boolean insertZanr(String naziv){
        boolean postoji=false;
        SQLiteDatabase db = this.getWritableDatabase();
        String[] koloneRezulat = new String[]{ GlumacDBOpenHelper.ZANR_ID, GlumacDBOpenHelper.ZANR_NAZIV};
        Cursor cursor = db.query(GlumacDBOpenHelper.DATABASE_TABLE3,
                koloneRezulat, null,
                null, null, null, null);

        if(cursor.moveToFirst()){
            do{
                if(cursor.getString(1).equals(naziv) ){
                    postoji=true;
                }
            }
            while(cursor.moveToNext());
        }
        if (cursor != null && !cursor.isClosed())
        {
            cursor.close();
        }
        if(postoji)
            return false;

        db.execSQL("INSERT INTO "+DATABASE_TABLE3+" ("+ZANR_NAZIV+") VALUES ('"+naziv+"');");
        return true;
    }
    public void insertRG(Integer reziser,Integer glumac ){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("INSERT INTO "+DATABASE_TABLE4+" ("+RG_GLUMACID+","+RG_REZISERID+") VALUES ('"+glumac+"' , '"+reziser+"');");

    }
    public void insertZG(Integer zanr,Integer glumac){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("INSERT INTO "+DATABASE_TABLE5+" ("+ZG_GLUMACID+","+ZG_ZANRID+") VALUES ('"+glumac+"' , '"+zanr+"');");

    }
    public boolean insertGlumac(String imeiprezime, String rodjenje, String smrt, String spol, String web, String
            biografija,String mjesto,String rejting, String image)
    {
        Boolean postoji=false;
        SQLiteDatabase db = this.getWritableDatabase();
        String[] koloneRezulat = new String[]{ GlumacDBOpenHelper.GLUMAC_ID, GlumacDBOpenHelper.GLUMAC_IMEIPREZIME, GlumacDBOpenHelper.GLUMAC_RODJENJE,
                GlumacDBOpenHelper.GLUMAC_SMRT, GlumacDBOpenHelper.GLUMAC_SPOL, GlumacDBOpenHelper.GLUMAC_WEB,
                GlumacDBOpenHelper.GLUMAC_BIOGRAFIJA,GlumacDBOpenHelper.GLUMAC_MJESTO,GlumacDBOpenHelper.GLUMAC_REJTING,
                GlumacDBOpenHelper.GLUMAC_IMAGE};
        Cursor cursor = db.query(GlumacDBOpenHelper.DATABASE_TABLE1,
                koloneRezulat, null,
                null, null, null, null);

        if(cursor.moveToFirst()){
            do{
                if(cursor.getString(1).equals(imeiprezime) ){
                    postoji=true;
                }
            }
            while(cursor.moveToNext());
        }
        if (cursor != null && !cursor.isClosed())
        {
            cursor.close();
        }
        if(postoji)
            return false;

            db.execSQL("INSERT INTO " + DATABASE_TABLE1 + " (" + GLUMAC_IMEIPREZIME + "," + GLUMAC_RODJENJE + "," + GLUMAC_SMRT + "," +
                     GLUMAC_SPOL + "," + GLUMAC_WEB + "," + GLUMAC_BIOGRAFIJA + "," + GLUMAC_MJESTO + "," + GLUMAC_REJTING + ","
                    + GLUMAC_IMAGE + ") VALUES ('" + imeiprezime + "' , '" + rodjenje + "' , '" + smrt + "' , '" + spol + "' , '" + web + "' , '" + biografija + "' , '" + mjesto + "' , '" + rejting + "' , '" + image + "');");


        return  true;


    }
}
