package ba.unsa.etf.rma.ehvan.projekat2;

import android.app.Fragment;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;
import android.os.Handler;

/**
 * Created by lion on 4/12/17.
 */

public class FragmentListaGlumaca extends Fragment implements  CustomResultReceiver.Receiver{
    OnItemClick oic;
    private ArrayList<Glumac> glumci;
    boolean baza= false;

    public CustomResultReceiver.Receiver funkcija(){
        return FragmentListaGlumaca.this;
    }
    public ArrayList<Glumac> getGlumci() {
        return glumci;
    }

    public void setGlumci(ArrayList<Glumac> glumci) {
        this.glumci = glumci;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup containter, Bundle savedInstanceState){
        return inflater.inflate(R.layout.listaglumaca_fragment,containter,false);
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        Button dugme=(Button) getView().findViewById(R.id.Pretrazi);
        Button dugme1=(Button) getView().findViewById(R.id.PretragaGuBazi);
        Button dugme2=(Button) getView().findViewById(R.id.PretragaGpomRuBazi);
        final EditText tekst=(EditText)getView().findViewById(R.id.pretraga);

        dugme.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                glumci.removeAll(glumci);
                Intent intent = new Intent(Intent.ACTION_SYNC, null, getActivity(), ActorsIntentService.class);
                CustomResultReceiver mReceiver = new CustomResultReceiver(new Handler());
                mReceiver.setReceiver(funkcija());
                intent.putExtra("ime",tekst.getText().toString());
                intent.putExtra("receiver", mReceiver);
                getActivity().startService(intent);
                //new SearchActors((SearchActors.onActorSearchDone) FragmentListaGlumaca.this).execute(tekst.getText().toString());
            }
        });
        dugme1.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                baza=true;
                glumci.removeAll(glumci);
                GlumacDBOpenHelper pom = new GlumacDBOpenHelper(getActivity(), "tabela_glumaca", null, 1);
                SQLiteDatabase db1 = pom.getWritableDatabase();
                String[] koloneRezulat1 = new String[]{GlumacDBOpenHelper.GLUMAC_ID, GlumacDBOpenHelper.GLUMAC_IMEIPREZIME, GlumacDBOpenHelper.GLUMAC_RODJENJE,
                        GlumacDBOpenHelper.GLUMAC_SMRT, GlumacDBOpenHelper.GLUMAC_SPOL, GlumacDBOpenHelper.GLUMAC_WEB,
                        GlumacDBOpenHelper.GLUMAC_BIOGRAFIJA, GlumacDBOpenHelper.GLUMAC_MJESTO, GlumacDBOpenHelper.GLUMAC_REJTING,
                        GlumacDBOpenHelper.GLUMAC_IMAGE};
                Cursor cursorg = db1.query(GlumacDBOpenHelper.DATABASE_TABLE1,
                        koloneRezulat1, null,
                        null, null, null, null);
                if (cursorg.moveToFirst()) {
                    do {



                        if (cursorg.getString(1).toLowerCase().contains(tekst.getText().toString().toLowerCase())) {
                            Glumac g = new Glumac(cursorg.getString(1), cursorg.getString(2), cursorg.getString(3), cursorg.getString(4),cursorg.getString(5), cursorg.getString(6), cursorg.getString(7), cursorg.getString(8));
                            g.setImage(cursorg.getString(9));
                            glumci.add(g);

                        }
                        //Glumac g = new Glumac(imeiprezime, rodjenje, smrt, spol, webStranica, biografija, mjesto, rejting);
                    }
                    while (cursorg.moveToNext());
                }
                if (cursorg != null && !cursorg.isClosed()) {
                    cursorg.close();
                }
                pom.close();
                ListView lv=(ListView)getView().findViewById(R.id.list);
                GlumacAdapter ga=new GlumacAdapter(getActivity(),glumci);
                lv.setAdapter(ga);


                try{
                    oic=(OnItemClick)getActivity();
                }
                catch (ClassCastException e){
                    throw  new ClassCastException(getActivity().toString()+"Treba implementirati OnItemClick");

                }
                lv.setOnItemClickListener(new AdapterView.OnItemClickListener(){
                    @Override
                    public void onItemClick(AdapterView<?> parent,View view,int position,long id){
                        oic.onItemClicked(position);
                    }
                });




            }
        });
        dugme2.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                baza=true;
                glumci.removeAll(glumci);
                GlumacDBOpenHelper pom = new GlumacDBOpenHelper(getActivity(), "tabela_glumaca", null, 1);
                SQLiteDatabase db1 = pom.getWritableDatabase();
                String[] koloneRezulat = new String[]{GlumacDBOpenHelper.REZISER_ID, GlumacDBOpenHelper.REZISER_IMEIPREZIME};
                Cursor cursor=db1.query(GlumacDBOpenHelper.DATABASE_TABLE2, koloneRezulat, null, null, null, null, null);
                String[] kolone = new String[]{GlumacDBOpenHelper.RG_ID, GlumacDBOpenHelper.RG_GLUMACID,GlumacDBOpenHelper.RG_REZISERID};
                Cursor c=db1.query(GlumacDBOpenHelper.DATABASE_TABLE4, kolone, null, null, null, null, null);

               int ReziserID=0;
                ArrayList<Integer> glumciID=new ArrayList<Integer>();
                if (cursor.moveToFirst()) {
                    do {



                        if (cursor.getString(1).toLowerCase().contains(tekst.getText().toString().toLowerCase())) {
                            ReziserID=cursor.getInt(0);

                        }
                        //Glumac g = new Glumac(imeiprezime, rodjenje, smrt, spol, webStranica, biografija, mjesto, rejting);
                    }
                    while (cursor.moveToNext());
                }
                if (cursor != null && !cursor.isClosed()) {
                    cursor.close();
                }
                if (c.moveToFirst()) {
                    do {



                        if (c.getInt(2)==ReziserID) {
                            glumciID.add(c.getInt(1));

                        }
                        //Glumac g = new Glumac(imeiprezime, rodjenje, smrt, spol, webStranica, biografija, mjesto, rejting);
                    }
                    while (c.moveToNext());
                }
                if (c != null && !c.isClosed()) {
                    c.close();
                }

                String[] koloneRezulat1 = new String[]{GlumacDBOpenHelper.GLUMAC_ID, GlumacDBOpenHelper.GLUMAC_IMEIPREZIME, GlumacDBOpenHelper.GLUMAC_RODJENJE,
                        GlumacDBOpenHelper.GLUMAC_SMRT, GlumacDBOpenHelper.GLUMAC_SPOL, GlumacDBOpenHelper.GLUMAC_WEB,
                        GlumacDBOpenHelper.GLUMAC_BIOGRAFIJA, GlumacDBOpenHelper.GLUMAC_MJESTO, GlumacDBOpenHelper.GLUMAC_REJTING,
                        GlumacDBOpenHelper.GLUMAC_IMAGE};
                Cursor cursorg = db1.query(GlumacDBOpenHelper.DATABASE_TABLE1,
                        koloneRezulat1, null,
                        null, null, null, null);
                if (cursorg.moveToFirst()) {
                    do {



                        if (glumciID.contains(cursorg.getInt(0))) {
                            Glumac g = new Glumac(cursorg.getString(1), cursorg.getString(2), cursorg.getString(3), cursorg.getString(4),cursorg.getString(5), cursorg.getString(6), cursorg.getString(7), cursorg.getString(8));
                            g.setImage(cursorg.getString(9));
                            glumci.add(g);

                        }
                        //Glumac g = new Glumac(imeiprezime, rodjenje, smrt, spol, webStranica, biografija, mjesto, rejting);
                    }
                    while (cursorg.moveToNext());
                }
                if (cursorg != null && !cursorg.isClosed()) {
                    cursorg.close();
                }
                pom.close();
                ListView lv=(ListView)getView().findViewById(R.id.list);
                GlumacAdapter ga=new GlumacAdapter(getActivity(),glumci);
                lv.setAdapter(ga);


                try{
                    oic=(OnItemClick)getActivity();
                }
                catch (ClassCastException e){
                    throw  new ClassCastException(getActivity().toString()+"Treba implementirati OnItemClick");

                }
                lv.setOnItemClickListener(new AdapterView.OnItemClickListener(){
                    @Override
                    public void onItemClick(AdapterView<?> parent,View view,int position,long id){
                        oic.onItemClicked(position);
                    }
                });




            }
        });

        if(getArguments().containsKey("Alista") && baza==false){
            glumci=getArguments().getParcelableArrayList("Alista");
            ListView lv=(ListView)getView().findViewById(R.id.list);
            GlumacAdapter ga=new GlumacAdapter(getActivity(),glumci);
            lv.setAdapter(ga);


        try{
            oic=(OnItemClick)getActivity();
        }
        catch (ClassCastException e){
            throw  new ClassCastException(getActivity().toString()+"Treba implementirati OnItemClick");

        }
            lv.setOnItemClickListener(new AdapterView.OnItemClickListener(){
                @Override
                public void onItemClick(AdapterView<?> parent,View view,int position,long id){
                    oic.onItemClicked(position);
                }
            });
        }

    }
    public interface OnItemClick{
        public void onItemClicked(int pos);
    }


    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {


            switch (resultCode) {


                case ActorsIntentService.STATUS_RUNNING:

                    Toast.makeText(getActivity(), "Pretrazivanje u toku", Toast.LENGTH_LONG).show();
                    break;
                case ActorsIntentService.STATUS_FINISHED:
                    glumci.removeAll(glumci);
                    ArrayList<Glumac> rez = resultData.getParcelableArrayList("result");

                    for (Glumac g:rez
                            ) {
                        glumci.add(g);
                    }
                    ListView lv=(ListView)getView().findViewById(R.id.list);
                    GlumacAdapter ga=new GlumacAdapter(getActivity(),glumci);
                    lv.setAdapter(ga);
                    Toast.makeText(getActivity(), "Pretrazivanje gotovo", Toast.LENGTH_LONG).show();
                    break;
                case ActorsIntentService.STATUS_ERROR:

                    String error = resultData.getString(Intent.EXTRA_TEXT);
                    Toast.makeText(getActivity(), "Doslo je do greske!", Toast.LENGTH_LONG).show();
                    break;
            }

    }
}
