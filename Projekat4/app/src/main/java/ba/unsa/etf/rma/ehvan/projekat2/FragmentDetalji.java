package ba.unsa.etf.rma.ehvan.projekat2;

import android.app.Fragment;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.view.View;
import android.widget.Toast;


import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import static android.R.attr.fragment;
import static ba.unsa.etf.rma.ehvan.projekat2.GlumacDBOpenHelper.GLUMAC_ID;
import static ba.unsa.etf.rma.ehvan.projekat2.GlumacDBOpenHelper.REZISER_ID;
import static ba.unsa.etf.rma.ehvan.projekat2.GlumacDBOpenHelper.RG_GLUMACID;
import static ba.unsa.etf.rma.ehvan.projekat2.GlumacDBOpenHelper.RG_REZISERID;
import static ba.unsa.etf.rma.ehvan.projekat2.GlumacDBOpenHelper.ZANR_ID;
import static ba.unsa.etf.rma.ehvan.projekat2.GlumacDBOpenHelper.ZG_GLUMACID;
import static ba.unsa.etf.rma.ehvan.projekat2.GlumacDBOpenHelper.ZG_ZANRID;

/**
 * Created by lion on 4/12/17.
 */

public class FragmentDetalji extends Fragment{
    private Glumac glumac;
    private ArrayList<Zanr>  zanrovi = new ArrayList<Zanr>();
    private ArrayList<Reziser> reziseri = new ArrayList<Reziser>();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup containter, Bundle savedInstanceState) {
        View iv = inflater.inflate(R.layout.detalji_fragment, containter, false);
        if(savedInstanceState!=null){
            glumac=savedInstanceState.getParcelable("parametri");
            zanrovi=savedInstanceState.getParcelableArrayList("zanrovi");
            reziseri=savedInstanceState.getParcelableArrayList("reziseri");
        }
        if(getArguments()!=null && getArguments().containsKey("Blista")){
            zanrovi=getArguments().getParcelableArrayList("Blista");
        }
        if(getArguments()!=null && getArguments().containsKey("Clista")){
            reziseri=getArguments().getParcelableArrayList("Clista");
        }
        if (getArguments() != null && getArguments().containsKey("glumac")) {
            glumac = getArguments().getParcelable("glumac");
            final TextView imeiprezime = (TextView) iv.findViewById(R.id.atextView1);
            imeiprezime.setText(glumac.getImeiPrezime());
            final TextView rodjenje = (TextView) iv.findViewById(R.id.atextView2);
            rodjenje.setText("Rodjen-a:" + " " + glumac.getRodjenje());
            final TextView smrt = (TextView) iv.findViewById(R.id.atextView3);
            if (glumac.getSmrt().equals("ziv"))
                smrt.setText("");
            else
                smrt.setText("Umro: " + glumac.getSmrt());
            final TextView biografija = (TextView) iv.findViewById(R.id.atextView4);
            biografija.setText("O glumcu-ici: " + glumac.getBiografija() + " Rejting: " + glumac.getRejting());
            final TextView mjesto = (TextView) iv.findViewById(R.id.atextView5);
            mjesto.setText("Mjesto rodjenja: " + glumac.getMjesto());
            final TextView spol = (TextView) iv.findViewById(R.id.atextView6);
            RelativeLayout l = (RelativeLayout) iv.findViewById(R.id.detalji);
            if (glumac.getSpol().equals("Musko")) {
                spol.setText("Spol: Musko");
                l.setBackgroundColor(Color.BLUE);


            } else {
                spol.setText("Spol: Zensko");
                l.setBackgroundColor(Color.RED);

            }

            final TextView stranica = (TextView) iv.findViewById(R.id.atextView7);
            stranica.setText(glumac.getWebStranica());
            final ImageView slika = (ImageView) iv.findViewById(R.id.aimageView);
            Picasso.with(getActivity().getApplicationContext())
                    .load(glumac.getImage())
                    .resize(50, 50)
                    .centerCrop()
                    .into(slika);
            stranica.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String url = glumac.getWebStranica();
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    if (i.resolveActivity(getActivity().getPackageManager()) != null) {
                        startActivity(i);
                    }
                }
            });
            Button dugme = (Button) iv.findViewById(R.id.dbutton);
            dugme.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {

                    String textMessage = glumac.getBiografija();
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, textMessage);
                    sendIntent.setType("text/plain");
                    if (sendIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                        startActivity(sendIntent);
                    }
                }
            });

            Button dugme1 = (Button) iv.findViewById(R.id.bookmark);
            Button dugme2 = (Button) iv.findViewById(R.id.remove_bookmark);

            boolean postoji = false;

            GlumacDBOpenHelper pom = new GlumacDBOpenHelper(getActivity(), "tabela_glumaca", null, 1);
            SQLiteDatabase db = pom.getWritableDatabase();
            String[] koloneRezulat = new String[]{GlumacDBOpenHelper.GLUMAC_ID, GlumacDBOpenHelper.GLUMAC_IMEIPREZIME, GlumacDBOpenHelper.GLUMAC_RODJENJE,
                    GlumacDBOpenHelper.GLUMAC_SMRT, GlumacDBOpenHelper.GLUMAC_SPOL, GlumacDBOpenHelper.GLUMAC_WEB,
                    GlumacDBOpenHelper.GLUMAC_BIOGRAFIJA, GlumacDBOpenHelper.GLUMAC_MJESTO, GlumacDBOpenHelper.GLUMAC_REJTING,
                    GlumacDBOpenHelper.GLUMAC_IMAGE};
            Cursor cursor = db.query(GlumacDBOpenHelper.DATABASE_TABLE1,
                    koloneRezulat, null,
                    null, null, null, null);

            if (cursor.moveToFirst()) {
                do {
                    if (cursor.getString(1).equals(glumac.getImeiPrezime())) {
                        postoji = true;
                    }
                }
                while (cursor.moveToNext());
            }

            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
            if (postoji) {
                dugme1.setVisibility(View.GONE);
                dugme2.setVisibility(View.VISIBLE);
            } else {
                dugme2.setVisibility(View.GONE);
                dugme1.setVisibility(View.VISIBLE);
            }

                dugme1.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {


                        GlumacDBOpenHelper pom = new GlumacDBOpenHelper(getActivity(), "tabela_glumaca", null, 1);
                        //vrati true ako ne postoji u bazi glumac
                        pom.insertGlumac(glumac.getImeiPrezime(), glumac.getRodjenje(), glumac.getSmrt(), glumac.getSpol(),
                                glumac.getWebStranica(), glumac.getBiografija(), glumac.getMjesto(), glumac.getRejting(), glumac.getImage());
                        SQLiteDatabase db1 = pom.getWritableDatabase();
                        String[] koloneRezulat1 = new String[]{GlumacDBOpenHelper.GLUMAC_ID, GlumacDBOpenHelper.GLUMAC_IMEIPREZIME, GlumacDBOpenHelper.GLUMAC_RODJENJE,
                                GlumacDBOpenHelper.GLUMAC_SMRT, GlumacDBOpenHelper.GLUMAC_SPOL, GlumacDBOpenHelper.GLUMAC_WEB,
                                GlumacDBOpenHelper.GLUMAC_BIOGRAFIJA, GlumacDBOpenHelper.GLUMAC_MJESTO, GlumacDBOpenHelper.GLUMAC_REJTING,
                                GlumacDBOpenHelper.GLUMAC_IMAGE};
                        Cursor cursorg = db1.query(GlumacDBOpenHelper.DATABASE_TABLE1,
                                koloneRezulat1, null,
                                null, null, null, null);
                        int brojglumaca=0;
                        if(cursorg.moveToLast()){
                            brojglumaca=cursorg.getInt(0);
                        }
                        if (cursorg != null && !cursorg.isClosed()) {
                            cursorg.close();
                        }
                        if(!reziseri.isEmpty()){
                       for (Reziser r : reziseri) {
                            boolean postoji = false;
                            int idisti=0;
                            SQLiteDatabase db = pom.getWritableDatabase();
                            String[] koloneRezulat = new String[]{REZISER_ID, GlumacDBOpenHelper.REZISER_IMEIPREZIME};
                            Cursor cursor = db.query(GlumacDBOpenHelper.DATABASE_TABLE2,
                                    koloneRezulat, null,
                                    null, null, null, null);

                            if (cursor.moveToFirst()) {
                                do {
                                    if (cursor.getString(1).equals(r.getImeiPrezime())) {
                                        postoji = true;
                                        idisti=cursor.getInt(0);
                                    }
                                }
                                while (cursor.moveToNext());
                            }

                           int brojrezisera = 0;
                           if(cursor.moveToLast()){
                               brojrezisera=cursor.getInt(0);
                           }
                            if (postoji) {
                                pom.insertRG( idisti,brojglumaca );
                            } else {
                                pom.insertReziser(r.getImeiPrezime());
                                if(brojrezisera==0){
                                    Cursor cursor1 = db.query(GlumacDBOpenHelper.DATABASE_TABLE2,
                                            koloneRezulat, null,
                                            null, null, null, null);
                                    if(cursor1.moveToLast())
                                        brojrezisera=cursor1.getInt(0);
                                    if (cursor1 != null && !cursor1.isClosed()) {
                                        cursor1.close();
                                    }
                                    pom.insertRG( brojrezisera ,brojglumaca );
                                }
                               else
                                    pom.insertRG( brojrezisera+1 ,brojglumaca );

                            }
                            if (cursor != null && !cursor.isClosed()) {
                                cursor.close();
                            }

                        }}
                        if(!zanrovi.isEmpty()){
                        for (Zanr z : zanrovi) {
                            boolean postoji = false;
                            int idisti=0;
                            SQLiteDatabase db = pom.getWritableDatabase();
                            String[] koloneRezulat = new String[]{ZANR_ID, GlumacDBOpenHelper.ZANR_NAZIV};
                            Cursor cursor = db.query(GlumacDBOpenHelper.DATABASE_TABLE3,
                                    koloneRezulat, null,
                                    null, null, null, null);

                            if (cursor.moveToFirst()) {
                                do {
                                    if (cursor.getString(1).equals(z.getNaziv())) {
                                        postoji = true;
                                        idisti=cursor.getInt(0);
                                    }
                                }
                                while (cursor.moveToNext());
                            }
                            int brojzanrova = 0;
                            if(cursor.moveToLast()){
                                brojzanrova=cursor.getInt(0);
                            }


                            if (postoji) {
                                Log.i("message","Isti!!!");
                                pom.insertZG(idisti,brojglumaca );
                            } else {
                                pom.insertZanr(z.getNaziv());
                                if(brojzanrova==0){

                                        Cursor cursor1 = db.query(GlumacDBOpenHelper.DATABASE_TABLE3,
                                                koloneRezulat, null,
                                                null, null, null, null);
                                        if(cursor1.moveToLast())
                                            brojzanrova=cursor1.getInt(0);
                                        if (cursor1 != null && !cursor1.isClosed()) {
                                            cursor1.close();
                                        }
                                    pom.insertZG(brojzanrova,brojglumaca);
                                }
                                else
                                    pom.insertZG(brojzanrova+1,brojglumaca);

                            }
                            if (cursor != null && !cursor.isClosed()) {
                                cursor.close();
                            }
                        }
                    }
                    pom.close();
                    zanrovi.removeAll(zanrovi);
                    reziseri.removeAll(reziseri);
                        Toast.makeText(getActivity(), "Glumac je bookmark-ovan", Toast.LENGTH_LONG).show();}


                });
            dugme2.setOnClickListener(new View.OnClickListener(){
                public void onClick(View v) {
                    //GLUMAC_ID
                    int id=0;
                    GlumacDBOpenHelper p = new GlumacDBOpenHelper(getActivity(), "tabela_glumaca", null, 1);
                    SQLiteDatabase db = p.getWritableDatabase();
                    String[] koloneRezulat = new String[]{GlumacDBOpenHelper.GLUMAC_ID, GlumacDBOpenHelper.GLUMAC_IMEIPREZIME, GlumacDBOpenHelper.GLUMAC_RODJENJE,
                            GlumacDBOpenHelper.GLUMAC_SMRT, GlumacDBOpenHelper.GLUMAC_SPOL, GlumacDBOpenHelper.GLUMAC_WEB,
                            GlumacDBOpenHelper.GLUMAC_BIOGRAFIJA, GlumacDBOpenHelper.GLUMAC_MJESTO, GlumacDBOpenHelper.GLUMAC_REJTING,
                            GlumacDBOpenHelper.GLUMAC_IMAGE};
                    Cursor cursor = db.query(GlumacDBOpenHelper.DATABASE_TABLE1,
                            koloneRezulat, null,
                            null, null, null, null);

                    if (cursor.moveToFirst()) {
                        do {
                            if (cursor.getString(1).equals(glumac.getImeiPrezime())) {
                                id=cursor.getInt(0);
                            }
                        }
                        while (cursor.moveToNext());
                    }
                    if (cursor != null && !cursor.isClosed()) {
                        cursor.close();
                    }

                    String where=GLUMAC_ID+ "=" + id;
                    String whereArgs[]=null;
                    db.delete(GlumacDBOpenHelper.DATABASE_TABLE1,where,whereArgs);
                    //brise glumca iz tabele glumaca sa datim id-om
                    ArrayList<Integer>lista=new ArrayList<Integer>();
                    ArrayList<Integer>lista2=new ArrayList<Integer>();
                    String where1=RG_GLUMACID+ "=" + id;
                    String where2=ZG_GLUMACID+ "=" + id;
                    String[] kolone1 = new String[]{ GlumacDBOpenHelper.RG_ID, GlumacDBOpenHelper.RG_GLUMACID,GlumacDBOpenHelper.RG_REZISERID};
                    Cursor cursor1 = db.query(GlumacDBOpenHelper.DATABASE_TABLE4,
                            kolone1, null,
                            null, null, null, null);
                    String[] kolone2 =new String[]{GlumacDBOpenHelper.ZG_ID, ZG_GLUMACID, ZG_ZANRID};
                    Cursor cursor2 =db.query(GlumacDBOpenHelper.DATABASE_TABLE5,kolone2,null,null,null,null,null);


                    if(cursor1.moveToFirst()){
                        do{
                            if(cursor1.getInt(1)==id ){
                                Log.d("message","ulazi u petlju");
                                lista.add(cursor1.getInt(2));// u listu su svi id-ovi od rezisera s kojim je izbrisani glumac radio
                            }
                        }
                        while(cursor1.moveToNext());
                    }
                    if (cursor1 != null && !cursor1.isClosed())
                    {
                        cursor1.close();
                    }
                    if(cursor2.moveToFirst()){
                        do{
                            if(cursor2.getInt(1)==(id) ){
                                lista2.add(cursor2.getInt(2));// u listu su svi id-ovi od rezisera s kojim je izbrisani glumac radio
                            }
                        }
                        while(cursor2.moveToNext());
                    }
                    if (cursor2 != null && !cursor2.isClosed())
                    {
                        cursor2.close();
                    }
                    if(lista.isEmpty())
                        Log.i("message","prazna je");
                    db.delete(GlumacDBOpenHelper.DATABASE_TABLE4,where1,whereArgs);
                    db.delete(GlumacDBOpenHelper.DATABASE_TABLE5,where2,whereArgs);
                    for (Integer i:lista) {
                        Log.i("message","uslo je"+i);
                        String koji=RG_REZISERID+ "=" + i;
                        //String[] kol = new String[]{ GlumacDBOpenHelper.REZISER_ID, GlumacDBOpenHelper.REZISER_IMEIPREZIME};
                        Cursor kurs=db.query(GlumacDBOpenHelper.DATABASE_TABLE4,kolone1,koji,null,null,null,null);
                        if(kurs.getCount()==0){
                            Log.i("message","uslo je");
                            String gdje=REZISER_ID+ "=" + i;
                           // String[] reziser = new String[]{ GlumacDBOpenHelper.REZISER_ID, GlumacDBOpenHelper.REZISER_IMEIPREZIME};
                            db.delete(GlumacDBOpenHelper.DATABASE_TABLE2,gdje,null);

                        }
                        if (kurs != null && !kurs.isClosed())
                        {
                            kurs.close();
                        }

                    }
                    for (Integer i:lista2) {
                        String koji=ZG_ZANRID+ "=" + i;
                        //String[] kol = new String[]{ GlumacDBOpenHelper.REZISER_ID, GlumacDBOpenHelper.REZISER_IMEIPREZIME};
                        Cursor kurs=db.query(GlumacDBOpenHelper.DATABASE_TABLE5,kolone2,koji,null,null,null,null);
                        if(kurs.getCount()==0){
                            Log.i("message","uslo je");
                            String gdje=ZANR_ID+ "=" + i;
                            // String[] reziser = new String[]{ GlumacDBOpenHelper.REZISER_ID, GlumacDBOpenHelper.REZISER_IMEIPREZIME};
                            db.delete(GlumacDBOpenHelper.DATABASE_TABLE3,gdje,null);

                        }
                        if (kurs != null && !kurs.isClosed())
                        {
                            kurs.close();
                        }

                    }
                    p.close();
                    Toast.makeText(getActivity(), "Glumac nije vise bookmark-ovan", Toast.LENGTH_LONG).show();
                }
            });



            //staviti da je zadnji glumac na kojeg je cklicked 0 zbog toga aplikacija pada

        }
        return  iv;



    }
    @Override
    public void onSaveInstanceState(Bundle b) {
        super.onSaveInstanceState(b);
        b.putParcelable("parametri", glumac);
        b.putParcelableArrayList("reziseri",reziseri);
        b.putParcelableArrayList("zanrovi",zanrovi);
    }

}
